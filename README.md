# Installation

This project uses Slim Framework and Eloquent ORM.

pixon2

composer install

add .env

- index file is located inside /public folder. Check for .htaccess in that folder

- app configuration and dependencies are located inside /app folder.

- routes are located inside /app/routes.php
