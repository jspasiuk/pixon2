<?php

error_reporting(E_ALL); // Error/Exception engine, always use E_ALL
ini_set('ignore_repeated_errors', TRUE); // always use TRUE
ini_set('display_errors', FALSE); // Error/Exception display, use FALSE only in production environment or real server. Use TRUE in development environment
ini_set('log_errors', TRUE); // Error/Exception file logging engine.
ini_set('error_log', 'error.loh'); // Logging file path

#script that delete files and folder from an existing content
if (!empty($argv[1])) {
    parse_str($argv[1], $_GET);
}

$path = $_GET["path"];
$hash = $_GET["hash"];

$path = base64_decode($path);
$hash = base64_decode($hash);

$date = date('d-m-Y');

$generatedHash = md5($path . $date . '-G(e@^&p93HPg(3<t');

$result = array(
    "status" => true,
    "message" => ''
);

if($generatedHash === $hash) {

    $directory = escapeshellarg($path); 
    exec("rm -rf $directory",$result["shell_response"]);
    $result["path"] = $path;

} else {
    echo $generatedHash;
    echo $hash;
    echo $path;
    $result["status"] = false;
    $result["message"] = 'Sorry, I failed.';
}

header('Content-Type: application/json');
echo json_encode($result);

die();

function rrmdir($src) {
    if (file_exists($src)) {
        $dir = opendir($src);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    rrmdir($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    } else {
        return 'File doesnt exists';
    }
}