<?php

require_once (dirname(__DIR__) . '/../lib/vendor/autoload.php');
use anlutro\cURL\cURL as CURL;

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__) . '/../');
$dotenv->load();


$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
  'driver'    => 'mysql',
  'host' => $_ENV["dbhost"],
  'database' => $_ENV["dbname"],
  'username' => $_ENV["dbuser"],
  'password' => $_ENV["dbpass"],
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
  'prefix'    => '',
  ]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$capsule::table("chron_execution_log")->insert(array(
  "name" => "files-api"
));

$curl = new CURL;

$apiKey = $_ENV["TMDB_API_KEY"];
$apiConfig = $curl->get('https://api.themoviedb.org/3/configuration?api_key=' . $apiKey);
$apiConfig = json_decode($apiConfig->body,true);

$response["success"] = array();
$response["failure"] = array();


$servers = $capsule::table('encoding_server')->get();

foreach ($servers as $server) {
  
  $files = $curl->get($server->url . '?p=&p=Pe}`>qpEcDftVJs(}+XVH+rFv"jwXVvxJE2Xfe6aUP}89=;Hd#PNhsj3fsbeL-pVj`KwbX7bLZ&c(MhgK@/_=u]cj%S-A)EYv7bB?[{#LucJ!2v?vP]u#<tPk7&r%ua');
  $files = json_decode($files,true);

foreach ($files as $file) {

  $series = false;
  $fileName = pathinfo($file["name"], PATHINFO_FILENAME); // removes file extension
  
  $fileParts = explode('-',$fileName);
  $apiFinder = $fileParts[0];
  $apiID = $fileParts[1];
  $series = $fileParts[2];
  if(isset($fileParts[2])) {
    $series = true;
    //$season= explode('x',$fileParts[2])[0];
    //$episode= explode('x',$fileParts[2])[1];
  }

  $append_to_response = 'credits,similar,recommendations,videos,images,external_ids';

  switch ($apiFinder) {
    case 'TMDB':
      $existingContent = $capsule::table('content')->where("tmdb_id",$apiID)->first();
      if($series) {
        $url = "https://api.themoviedb.org/3/tv/".$apiID."?api_key=".$apiKey;
        $urlProvider = "https://api.themoviedb.org/3/tv/".$apiID."/watch/providers?api_key=".$apiKey;
      } else {
        $url = "https://api.themoviedb.org/3/movie/".$apiID."?api_key=".$apiKey;
        $urlProvider = "https://api.themoviedb.org/3/movie/".$apiID."/watch/providers?api_key=".$apiKey;
      }
      break;
    default:
      $url = "";
      break;
  }

  if(!$existingContent) {

    if($url) {
      $urlSpanish = $url . "&language=es&append_to_response=images"; //para spanish no necesitamos todo el contenido, asi ahorramos kb
      $urlEnglish = $url . "&append_to_response=" . $append_to_response . "&language=en"; //aca si apendeamos todo que necesitamos abajo
      $urlNoLang = $url .  "&language=xx&append_to_response=images&include_image_language=null"; //no language para backdrops

      $apiResponseSpanish = $curl->get($urlSpanish);
      $apiResponseEnglish = $curl->get($urlEnglish);
      $apiResponseNoLang = $curl->get($urlNoLang);

      $apiProviders = $curl->get($urlProvider);


      if($apiResponseEnglish->statusCode === 200) {
        $contentToCreate["en"] = json_decode($apiResponseEnglish->body,true);
        $contentToCreate["es"] = json_decode($apiResponseSpanish->body,true);
        $contentToCreate["xx"] = json_decode($apiResponseNoLang->body,true);
        
        
        $actors = array();

        try {
          //create content
          $contentID = $capsule::table("content")->insertGetId(array(
            "tmdb_id" => $contentToCreate["en"]["id"]
            ,"imdb_id" => $contentToCreate["en"]["external_ids"]["imdb_id"]
            ,"name" => $series ? $contentToCreate["en"]["name"] : $contentToCreate["en"]["title"]
            ,"name_es" => $series ? $contentToCreate["es"]["name"] : $contentToCreate["es"]["title"]
            ,"type" => $series? 'serie' : 'movie'
            ,"source" => ''
            ,"plot" => $contentToCreate["en"]["overview"]
            ,"plot_es" => $contentToCreate["es"]["overview"]
            ,"release_date" => $series ? $contentToCreate["en"]["first_air_date"] : $contentToCreate["en"]["release_date"]
            ,"tagline" => $contentToCreate["en"]["tagline"]
            ,"tagline_es" => $contentToCreate["es"]["tagline"]
            ,"imdb" => $contentToCreate["en"]["vote_average"]
          ));

          if($contentToCreate["en"]["videos"]["results"]) {
            foreach ($contentToCreate["en"]["videos"]["results"] as $video) {
              if($video["type"] == "Trailer" && $video["site"] == "YouTube") {
                $capsule::table("content")->where("id",$contentID)->update(array(
                  "trailer_id" => $video["key"]
                ));
              }
            }
          }

          if($contentToCreate["xx"]["backdrop_path"]) {
            $backdrop= $_ENV["public_img_url"] . downloadImage('http://image.tmdb.org/t/p/w1280' . $contentToCreate["xx"]["backdrop_path"]);
            if($backdrop) {
              $capsule::table("content_backdrop")->insert(
                array(
                  "content_id" => $contentID,
                  "url" => $backdrop
                )
              );
            }
          }

          isset($contentToCreate["xx"]["images"]["backdrops"])?downloadBackdrops($capsule,$contentID,$contentToCreate["xx"]["images"]["backdrops"]) : null;

          downloadPosters($capsule,$contentID,"en",$contentToCreate["en"]["images"]["posters"]);
          downloadPosters($capsule,$contentID,"es",$contentToCreate["es"]["images"]["posters"]);

          downloadLogos($capsule,$contentID,"en",$contentToCreate["en"]["images"]["logos"]);
          downloadLogos($capsule,$contentID,"es",$contentToCreate["es"]["images"]["logos"]);

          if($contentToCreate["en"]["poster_path"]) {
            downloadPosters($capsule,$contentID,"en",array(array("file_path" => $contentToCreate["en"]["poster_path"])));
          }

          if($contentToCreate["es"]["poster_path"]) {
            downloadPosters($capsule,$contentID,"es",array(array("file_path" => $contentToCreate["es"]["poster_path"])));
          }

          foreach ($contentToCreate["en"]["production_companies"]as $company) {
            if (createCompany($capsule,$company)) {
              $capsule::table("content_company")->insert(array(
                "content_id" => $contentID,
                "company_id" => $company["id"]
              ));
            }
          }          

          foreach ($contentToCreate["en"]["credits"]["cast"] as $actor) {
            if (createActor($capsule,$actor)) {
              $capsule::table("content_actor")->insert(array(
                "content_id" => $contentID,
                "actor_id" => $actor["id"],
                "job" => $actor["known_for_department"],
                "order" => $actor["order"]
              ));
            }
          }

          foreach ($contentToCreate["en"]["credits"]["crew"] as $actor) {
            //del crew solo traigo los directores
            if($actor["job"] == "Director") {
              if (createActor($capsule,$actor)) {
                $capsule::table("content_actor")->insert(array(
                  "content_id" => $contentID,
                  "actor_id" => $actor["id"],
                  "job" => $actor["job"]
                ));
              }
            }            
          }

          foreach ($contentToCreate["en"]["production_countries"] as $country) {
            $capsule::table("content_production_country")->insert(array(
              "content_id" => $contentID,
              "country_code" => $country["iso_3166_1"]
            ));
          }

          foreach ($contentToCreate["en"]["recommendations"]["results"] as $recommendation) {            
              $capsule::table("content_recommendation")->insert(array(
                "content_id" => $contentID,
                "recommendation_tmdb_id" => $recommendation["id"]
              ));            
          }

          foreach ($contentToCreate["en"]["similar"]["results"] as $similar) {            
            $capsule::table("content_similar")->insert(array(
              "content_id" => $contentID,
              "similar_tmdb_id" => $similar["id"]
            ));
          }

          foreach ($contentToCreate["es"]["genres"] as $key => $genre) {
            if (createGenre($capsule,$genre,$contentToCreate["es"]["genres"][$key])) {
              $capsule::table("content_genre")->insert(array(
                "content_id" => $contentID,
                "genre_id" => $genre["id"]
              ));
            }
          }


          if($apiProviders) {
            $providers = json_decode($apiProviders->body,true);
            if($providers["results"]["US"]) {
              if($providers["results"]["US"]["flatrate"]) {
                foreach ($providers["results"]["US"]["flatrate"] as $provider) {
                  if (createProvider($capsule,$provider)) {
                    $capsule::table("content_provider")->insert(array(
                      "content_id" => $contentID,
                      "provider_id" => $provider["provider_id"],
                    ));
                  }
                }
              }
            }
          }


          array_push($response["success"],array("id" => $contentID , "tmdb_id" => $contentToCreate["en"]["id"]));

        } catch (\Throwable $th) {

          array_push($response["failure"],array("tmdb_id" => $contentToCreate["en"]["id"], "error" => $th->getMessage()));

        }
        

      } else {
        //not found on tmdb api
      }

    } else {
      //not found
    }

  }

}
}

header('Content-Type: application/json');
echo json_encode($response);


///////////// funcs /////////////

function downloadBackdrops($capsule,$contentID,$backdrops) {

  $i = 1;
  if($backdrops) {
    foreach ($backdrops as $backdrop) {
      if ($backdropURL = downloadImage('http://image.tmdb.org/t/p/w1280' . $backdrop["file_path"])) {
        $capsule::table("content_backdrop")->insert(array(
          "content_id" => $contentID,
          "url" => $_ENV["public_img_url"] . $backdropURL
        ));
        if($i == 3) {break;} //only download 3 backdrops
        $i++;
      }
    }
  }
  
}


function downloadLogos($capsule,$contentID,$lang,$logos) {

  $i = 1;
  if($logos) {
    foreach ($logos as $logo) {
      if ($logoUrl = downloadImage('http://image.tmdb.org/t/p/w300' . $logo["file_path"])) {
        $capsule::table("content_logo")->insert(array(
          "content_id" => $contentID,
          "lang" => $lang,
          "url" => $_ENV["public_img_url"] . $logoUrl
        ));
      }
      if($i == 4) {break;} //only download 4 logos
      $i++;
    }
  }
  
}


function downloadPosters($capsule,$contentID,$lang,$posters) {

  $i = 1;
  if($posters) {
    foreach ($posters as $poster) {
      if ($posterURL = downloadImage('http://image.tmdb.org/t/p/w500' . $poster["file_path"])) {
        $capsule::table("content_poster")->insert(array(
          "content_id" => $contentID,
          "lang" => $lang,
          "url" => $_ENV["public_img_url"] . $posterURL
        ));
      }
      if($i == 4) {break;} //only download 4 posters
      $i++;
    }
  }
  
}

function downloadImage($imageUrl) {

  $content = file_get_contents($imageUrl);
  $imageName = uniqid() . ".jpg";
  file_put_contents($_ENV["uploads_dir"] . $imageName, $content);

  return $imageName;

}

function createGenre($capsule,$genre,$genreEs) {

  $result = $capsule::table("genre")->insertOrIgnore(array(
    "id" => $genre["id"],
    "name" => $genre["name"],
    "name_es" => $genreEs["name"]
  ));

  return $genre["id"];

}


function createCompany($capsule,$company) {  

  $result = $capsule::table("company")->insertOrIgnore(array(
    "id" => $company["id"],
    "name" => $company["name"],
    "country" => $company["origin_country"]
  ));

  if($result && $company["logo_path"]) {
    //uploadImage
    $image = downloadImage('http://image.tmdb.org/t/p/w300' . $company["logo_path"]);
    $capsule::table("company")->where("id",$company["id"])->update(array("logo" => $_ENV["public_img_url"] . $image));
  }

  return $company["id"];

}



function createActor($capsule,$actor) {  

  $result = $capsule::table("actor")->insertOrIgnore(array(
    "id" => $actor["id"],
    "name" => $actor["name"]
  ));

  if($result && $actor["profile_path"]) {
    //uploadImage
    $image = downloadImage('http://image.tmdb.org/t/p/w185' . $actor["profile_path"]);
    $capsule::table("actor")->where("id",$actor["id"])->update(array("image" => $_ENV["public_img_url"] . $image));
  }

  return $actor["id"];

}

function createProvider($capsule,$provider) {  

  $result = $capsule::table("providers")->insertOrIgnore(array(
    "id" => $provider["provider_id"],
    "name" => $provider["provider_name"]
  ));

  if($result && $provider["logo_path"]) {
    //uploadImage
    $image = downloadImage('http://image.tmdb.org/t/p/original' . $provider["logo_path"]);
    $capsule::table("providers")->where("id",$provider["provider_id"])->update(array("logo" => $_ENV["public_img_url"] . $image));
  }

  return $provider["provider_id"];

}


?>