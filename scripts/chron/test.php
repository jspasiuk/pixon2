<?php

require_once(__DIR__ . '/../../vendor/autoload.php');


$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__) . '/../');
$dotenv->load();


$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
  'driver'    => 'mysql',
  'host' => $_ENV["dbhost"],
  'database' => $_ENV["dbname"],
  'username' => $_ENV["dbuser"],
  'password' => $_ENV["dbpass"],
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
  'prefix'    => '',
  ]);
$capsule->setAsGlobal();
$capsule->bootEloquent();


$response = [];
$sliders = $capsule::select('SELECT * FROM `slider` where home_type = "movie"');

foreach ($sliders as $slider) {
    $items = [];


    $slider_items = $capsule::select('SELECT * FROM `slider_item` where slider_id = ?', [$slider->id]);
    if($slider_items) {
        //bring content in this items
    }
    $slider_genres = $capsule::select('SELECT * FROM `slider_genre` where slider_id = ?', [$slider->id]);
    if($slider_genre) {
        // bring sliders by genre
    }

}




?>