<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", "0");
ini_set("log_errors", 1);
ini_set("error_log", "inform.log");

if (!empty($argv[1])) {
  parse_str($argv[1], $_GET);
} else {
  die("dont come here");
}

$connection = array(
  "DB_NAME" => "changos",
  "DB_HOST" => "78.46.72.178",
  "DB_USERNAME" => "encoding",
  "DB_PASS" => "6c5f61ca-b380-11eb-adca-f45c89bbca99"
);

require 'vendor/autoload.php';

ORM::configure('mysql:host='.$connection["DB_HOST"].';dbname='.$connection["DB_NAME"].'');
ORM::configure('username', $connection["DB_USERNAME"]);
ORM::configure('password', $connection["DB_PASS"]);

$job = ORM::for_table("chron_process_list")->find_one($_GET["id"]);
$job->current_step = $_GET["step"];

if(isset($_GET["status"])) {  
  $job->status = $_GET["status"];
  if($_GET["status"] == "done") {
    //insert into content_video with video caracteristics
    $filenoext=explode(".",$job->filename)[0];
    $fileParts = explode('-',$job->filename);
    if($fileParts[2]) {
      $series = $fileParts[2];
    }

    $content_video = ORM::for_table('content_video')->create();
    $content_video->content_id = $job->content_id;
    $content_video->duration = $job->duration;
    $content_video->download_file = $job->filename;
    $content_video->folder= $filenoext;
    $content_video->filename= $filenoext . '-auto.mpd';
    if($series) {
      $content_video->season = explode('x',$series)[0];
      $content_video->episode = explode('.',explode('x',$series)[1])[0];
    }
    if($content_video->save()) {
      $job->sn_360 ? newVideoQuality($content_video->id,'SD',$filenoext . '-360.mpd') : null;
      $job->sn_720 ? newVideoQuality($content_video->id,'HD',$filenoext . '-720.mpd') : null;
      $job->sn_1080 ? newVideoQuality($content_video->id,'FULLHD',$filenoext . '-1080.mpd') : null;

      $job->audio_en ? newVideoAudio($content_video->id,'en') : null;
      $job->audio_es ? newVideoAudio($content_video->id,'es') : null;
      $job->audio_lat ? newVideoAudio($content_video->id,'lat') : null;

      $job->sub_es ? newVideoSubtitle($content_video->id,'es') : null;
      $job->sub_en ? newVideoSubtitle($content_video->id,'en') : null;
    }
  }

  if($_GET["status"] == "failed") {
    $job->error = $_GET["error"] ? $_GET["error"] : '';
    disableServer($job->server_id,$job->error);
  }

}

$job->save();

echo $_GET["file"] . " paso por php " . $_GET["step"] . PHP_EOL;

function disableServer($server_id,$reason) {

  $server = ORM::for_table("encoding_server")->find_one($server_id);
  $server->status = 0;
  $server->status_message = $reason;
  $server->save();;

  $jobs = ORM::for_table("chron_process_list")->where("server_id",$server_id)->where("status","pending")->find_many();
  foreach($jobs as $job) {
    $job->status = "stopped";
    $job->error = 'Server has been disabled. Enable server to put all this on pending again';
    $job->save();
  }


}

function newVideoSubtitle($video_id,$lang) {
  $content_audio = ORM::for_table("content_video_subtitle")->create();
  $content_audio->video_id = $video_id;
  $content_audio->lang = $lang;
  $content_audio->save();
}

function newVideoAudio($video_id,$lang) {
  $content_audio = ORM::for_table("content_video_audio")->create();
  $content_audio->video_id = $video_id;
  $content_audio->lang = $lang;
  $content_audio->save();
}

function newVideoQuality($video_id,$qualityName,$qualityFile) {
  $content_quality = ORM::for_table("content_video_quality")->create();
  $content_quality->video_id = $video_id;
  $content_quality->name = $qualityName;
  $content_quality->filename = $qualityFile;
  $content_quality->save();
}

?>