<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", "0");
ini_set("log_errors", 1);
ini_set("error_log", "filesinfo.log");

if(!$_GET["p"] === 'Pe}`>qpEcDftVJs(}+XVH+rFv"jwXVvxJE2Xfe6aUP}89=;Hd#PNhsj3fsbeL-pVj`KwbX7bLZ&c(MhgK@/_=u]cj%S-A)EYv7bB?[{#LucJ!2v?vP]u#<tPk7&r%ua') {
  exit;
}

require 'vendor/autoload.php';

$path = '/var/www/files/';

$ffprobe = FFMpeg\FFProbe::create();

$files = glob($path.'*.{mkv,mp4}', GLOB_BRACE);

$return = array();

foreach($files as $file) {

  $temp = array(
    "name" => $file,
    "duration" => 0,
    "streams" => array(
      "subtitle" => array(),
      "audio" => array()
    )
  );

  $temp["duration"] = intval($ffprobe->format($file)->get('duration'));
  $streams = $ffprobe->streams($file);
  

  foreach ($streams as $stream) {

    $codec_type = $stream->get("codec_type");

    switch ($codec_type ) {
      case 'video':
        if($stream->get("index") === 0){
          $temp["streams"]["video"] = array(
            "index" => $stream->get("index"),
            "width" => $stream->get('width'),
            "height" => $stream->get('height')
          );  
        }
      break;
      case 'audio':
        array_push($temp["streams"]["audio"],array(
          "index" => $stream->get("index"),
          "language" => $stream->get("tags")["language"],
          "title" => $stream->get("tags")["title"],
          "description" => $stream->get("codec_type") . " - " . $stream->get("codec_long_name") . " - Duration: " . $stream->get("duration")
        ));
  
      break;
      case 'subtitle':
        $disposition = $stream->get('disposition');
        if($disposition["forced"] === 1
        || stristr($stream->get("tags")["title"],'Forzados')
        || stristr($stream->get("tags")["title"],'Forced')
        || stristr($stream->get("codec_long_name"),'HDMV')
        ) {
         
        } else {
          array_push($temp["streams"]["subtitle"],array(
            "index" => $stream->get("index"),
            "language" => $stream->get("tags")["language"],
            "title" => $stream->get("tags")["title"],
            "description" => $stream->get("codec_long_name") . " - Duration: " . $stream->get("duration") . ($disposition["forced"] ? ' (forced)' : '')
          ));
        }
      break;
      default:
        # code...
        break;
    }
 
  }

  array_push($return,$temp);

}

header('Content-Type: application/json');
echo json_encode($return);
die();


function arrayToText($array) {
  $text = "";
  foreach ($array as $key => $value) {
    $text .= $value . ", ";
  }
  return $text;
}