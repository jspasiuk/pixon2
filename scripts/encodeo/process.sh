#!/bin/bash
TMP="tmp"
BENTO4PATH="/usr/bin/bento4/bin"
SPACE=" "
INPUT_FOLDER="files"

SERVER_DEST="195.201.160.22"
USER_DEST="transfer2"
FOLDER_DEST="/var/www/html/chango/video"
KEY_FILE="keys/transferContent.pem"

DB_HOST="78.46.72.178"
DB_USERNAME="encoding"
DB_PASS="6c5f61ca-b380-11eb-adca-f45c89bbca99"
DB_NAME="changos"

SERVER_ID=3

while IFS='\n' read -r line; do

    id=$(echo $line | awk '{print $1}')
    filename=$(echo $line | awk '{print $2}')
    sn1080=$(echo $line | awk '{print $3}')
    sn720=$(echo $line | awk '{print $4}')
    sn360=$(echo $line | awk '{print $5}')
    audioen=$(echo $line | awk '{print $6}')
    audioes=$(echo $line | awk '{print $7}')
    audiolat=$(echo $line | awk '{print $8}')
    subes=$(echo $line | awk '{print $9}')
    suben=$(echo $line | awk '{print $10}')

    FILENAME_NOEXT=${filename%.*}
    LOG=log/$FILENAME_NOEXT
    OUTPUT_FOLDER="output"/$FILENAME_NOEXT
    FULLPATH=$INPUT_FOLDER/$filename

    if [ ! -f $FULLPATH ]; then
      echo "File not found!"
      php inform.php "id=$id&file=$filename&step=0&status=failed&error=file not found!"
      continue
    fi

    php inform.php "id=$id&file=$filename&step=0&status=doing"

    rm $TMP -r -f
    rm $LOG -r -f
    rm $OUTPUT_FOLDER -r -f
    mkdir $LOG
    mkdir $TMP
    mkdir $TMP/video
    mkdir $TMP/audio
    mkdir $TMP/subtitle
    mkdir 'output'
    mkdir $OUTPUT_FOLDER

    MANIFEST_PART=""
    SUBPART=""

    if [ ! "$audioen" == "NULL" ]
    then
      MANIFEST_AUDIO_EN="${TMP}/audio/${filename}-f-audio-en.mp4"
      php inform.php "id=$id&file=$filename&step=1"
      ffmpeg -nostdin -nostdin -i "$FULLPATH" -map 0:$audioen -ac 2 -c:a aac -vn -sn $TMP/audio/$filename-audio-en.mp4> $LOG/$filename-audio-en.txt 2>&1
      php inform.php "id=$id&file=$filename&step=2"
      $BENTO4PATH/mp4fragment $TMP/audio/$filename-audio-en.mp4 "$MANIFEST_AUDIO_EN"
      MANIFEST_PART=$MANIFEST_PART$SPACE$MANIFEST_AUDIO_EN
    fi

    if [ ! "$audioes"  == "NULL" ]
    then  
      MANIFEST_AUDIO_ES="${TMP}/audio/${filename}-f-audio-es.mp4"
      php inform.php "id=$id&file=$filename&step=3"
      ffmpeg  -nostdin -i "$FULLPATH" -map 0:$audioes -ac 2 -c:a aac -vn -sn $TMP/audio/$filename-audio-es.mp4> $LOG/$filename-audio-es.txt 2>&1
      php inform.php "id=$id&file=$filename&step=4"
      $BENTO4PATH/mp4fragment $TMP/audio/$filename-audio-es.mp4 "$MANIFEST_AUDIO_ES"
      MANIFEST_PART=$MANIFEST_PART$SPACE$MANIFEST_AUDIO_ES
    fi

    if [ ! "$audiolat"  == "NULL" ]
    then  
      MANIFEST_AUDIO_LAT="${TMP}/audio/${filename}-f-audio-lat.mp4"
      php inform.php "id=$id&file=$filename&step=5"
      ffmpeg -nostdin -i "$FULLPATH" -map 0:$audiolat -ac 2 -c:a aac -vn -sn $TMP/audio/$filename-audio-lat.mp4> $LOG/$filename-audio-lat.txt 2>&1
      php inform.php "id=$id&file=$filename&step=6"
      $BENTO4PATH/mp4fragment $TMP/audio/$filename-audio-lat.mp4 $MANIFEST_AUDIO_LAT
      MANIFEST_PART=$MANIFEST_PART$SPACE$MANIFEST_AUDIO_LAT
    fi

    if [ ! "$subes"  == "NULL" ]
    then
      SUB_ES="${TMP}/subtitle/${FILENAME_NOEXT}-subtitle-es.vtt"
      php inform.php "id=$id&file=$filename&step=7"
      ffmpeg -nostdin -i "$FULLPATH" -map "0:$subes" -vn -an "$SUB_ES"> $LOG/subtitle-es.txt 2>&1
      SUBPART="[+format=webvtt,+language=es]${SUB_ES}"
    fi

    if [ ! "$suben"  == "NULL" ]
    then
      SUB_EN="${TMP}/subtitle/${FILENAME_NOEXT}-subtitle-en.vtt"
      php inform.php "id=$id&file=$filename&step=8"
      ffmpeg -nostdin -i "$FULLPATH" -map "0:$suben" -vn -an "$SUB_EN"> $LOG/subtitle-en.txt 2>&1
      SUBPART=$SPACE" ${SUBPART} [+format=webvtt,+language=en]${SUB_EN}"
    fi

  #####begin video#####
  #: <<'END'
  #END

    if [ "$sn1080" == "1" ]
    then
      BENTO_1080_OUTPUT=$TMP/video/f-$filename-1080.mp4
      php inform.php "id=$id&file=$filename&step=9"
      ffmpeg -nostdin -hwaccel auto -i "$FULLPATH" -an -sn -c:0 h264_nvenc -b:v 2500k -maxrate 2500k -bufsize 1250k $TMP/video/$filename-1080.mp4> $LOG/$filename-1.txt 2>&1
      php inform.php "id=$id&file=$filename&step=10"
      $BENTO4PATH/mp4fragment $TMP/video/$filename-1080.mp4 $BENTO_1080_OUTPUT
      $BENTO4PATH/mp4dash --output-dir="$OUTPUT_FOLDER" --force --mpd-name=$FILENAME_NOEXT-1080.mpd --profiles=on-demand "${TMP}/video/f-${filename}-1080.mp4" $MANIFEST_PART $SUBPART
    fi
    if [ "$sn720" == "1" ]
    then
      BENTO_720_OUTPUT=$TMP/video/f-$filename-720.mp4
      php inform.php "id=$id&file=$filename&step=11"
      ffmpeg -nostdin -hwaccel auto -i "$FULLPATH" -an -sn -c:0 h264_nvenc -b:v 1500k -maxrate 1500k -bufsize 1250k -vf scale=1280:-2 $TMP/video/$filename-720.mp4> $LOG/$filename-2.txt 2>&1
      php inform.php "id=$id&file=$filename&step=12"
      $BENTO4PATH/mp4fragment $TMP/video/$filename-720.mp4 $BENTO_720_OUTPUT
      $BENTO4PATH/mp4dash --output-dir="$OUTPUT_FOLDER" --force --mpd-name=$FILENAME_NOEXT-720.mpd --profiles=on-demand $BENTO_720_OUTPUT $MANIFEST_PART $SUBPART

    fi

    if [ "$sn360" == "1" ]
    then
      BENTO_360_OUTPUT=$TMP/video/f-$filename-360.mp4
      php inform.php "id=$id&file=$filename&step=13"  
      ffmpeg -nostdin -hwaccel auto -i "$FULLPATH" -an -sn -c:0 h264_nvenc -b:v 500k -maxrate 500k -bufsize 250k -vf scale=640:-2 $TMP/video/$filename-360.mp4> $LOG/$filename-3.txt 2>&1
      php inform.php "id=$id&file=$filename&step=14"
      $BENTO4PATH/mp4fragment $TMP/video/$filename-360.mp4 $BENTO_360_OUTPUT
      $BENTO4PATH/mp4dash --output-dir="$OUTPUT_FOLDER" --force --mpd-name=$FILENAME_NOEXT-360.mpd --profiles=on-demand $BENTO_360_OUTPUT $MANIFEST_PART $SUBPART
    
    fi

    php inform.php "id=$id&file=$filename&step=15"
    $BENTO4PATH/mp4dash --output-dir="$OUTPUT_FOLDER" --force --mpd-name=$FILENAME_NOEXT-auto.mpd --profiles=on-demand $BENTO_1080_OUTPUT $BENTO_720_OUTPUT $BENTO_360_OUTPUT $MANIFEST_PART $SUBPART

    php inform.php "id=$id&file=$filename&step=16"

    #muevo el mkv donde corresponde
    mv $FULLPATH $OUTPUT_FOLDER/

    scp -q -r -i "${KEY_FILE}" "${OUTPUT_FOLDER}" "${USER_DEST}"@"${SERVER_DEST}":"${FOLDER_DEST}"

    #chequeo que se haya subido
    if [ $ ? -ne 0 ]
    then
      echo 'copy to transfer failed, check error in scp command'
      php inform.php "id=$id&file=$filename&step=17&status=failed&error=SCP to transfer server failed!!"
      exit 1
      #borro el archivo de la carpeta temporal
    fi

    ssh -i "${KEY_FILE}" "${USER_DEST}"@"${SERVER_DEST}" chmod  -R 775 ${FOLDER_DEST}"/${FILENAME_NOEXT}"

    php inform.php "id=$id&file=$filename&step=17&status=done"

    rm $TMP -r -f
    rm $OUTPUT_FOLDER -r -f

done< <(mysql -N -e "SELECT id,filename,sn_1080,sn_720,sn_360,audio_en,audio_es,audio_lat,sub_es,sub_en FROM $DB_NAME.chron_process_list where status= 'pending' and server_id=$SERVER_ID order by id asc LIMIT 1" -h $DB_HOST -u $DB_USERNAME -p$DB_PASS)

exit 1
