<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\Profile;
use DateTime;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use Exception;
use SendGrid;

class UserController extends Controller
{


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getUsersFiltered($request,$response) {

        $params = $request->getQueryParams();

        $users = User::select("id","username","active","credit_expires","maxDevices","saler_id","created_at","updated_at","comment")
        ->whereNull("is_admin")
        ->orderBy("created_at","desc");

        if($params["email"]) {
            $users = $users->where("username","like","%" . $params["email"] . "%");
        }

        if($params["saler"]) {
            $users = $users->whereIn("saler_id",explode(',',$params["saler"]));
        }

        $users = $users->limit(200)->get();

        return $this->pxApiResponse($response,true,$users,'List',200);        
    }

    public function getUsersForSaler($request,$response,$args) {

        $users = User::select("id","username","active","credit_expires","maxDevices","saler_id","created_at","updated_at","comment")
        ->where("saler_id",$args["saler_id"])
        ->whereNull("is_admin")
        ->orderBy("created_at","desc")
        ->get();

        return $this->pxApiResponse($response,true,$users,'List',200);        
    }

    public function newUsersForSaler($request,$response,$args) {
        $parsedBody = $request->getParsedBody();

        if(!$parsedBody["email"] || !$parsedBody["saler_id"]) {
            return $this->pxApiResponse($response,false,[],'Missing params email, saler_id',200);
        }

        try {
            $existingUser = User::where("username",$parsedBody["email"])->first();
            $user = $existingUser ? $existingUser : new User();
            if($parsedBody["days"]) {
                $days = intval($parsedBody["days"]);
                $days = $days > 0 ? '+ '.$days : '- '.$days;
                if($user->credit_expires) {

                    $dateUntilCreditsExpires = date($user->credit_expires);
                    if($dateUntilCreditsExpires< date('Y-m-d H:i:s')) {
                        $dateUntilCreditsExpires = date('Y-m-d H:i:s');
                    }

                    $user->credit_expires = Date('Y-m-d', strtotime($dateUntilCreditsExpires . $days.' days'));    
                } else {
                    $user->credit_expires = Date('Y-m-d', strtotime($days.' days'));    
                }
            }

            $user->active = $parsedBody["enabled"] ? 1 : 0;
            $user->saler_id = $parsedBody["saler_id"];
            $user->username = trim($parsedBody["email"]);
            $user->maxDevices = 10; //TODO
            $user->screens = $parsedBody["screens"];
            $user->comment = $parsedBody["comment"];

            $user->save();
            return $this->pxApiResponse($response,true,["id" => $user->id],'OK',200);        
            
        } catch (\Throwable $th) {
            return $this->pxApiResponse($response,false,[],'Error saving user in DB',200);        
        }
    }

    public function deleteUserForSaler($request,$response,$args) {

    }

    public function newProfile($request,$response) {
        $parsedBody = $request->getParsedBody();
        $decoded = $request->getAttribute("token");

      //check for credits
      if($user = User::find($decoded["user_id"])) {

        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->name = $parsedBody["name"];
        $profile->image = $parsedBody["image"];
        $profile->lang = $parsedBody["lang"];


        if($profile->save()) {
            return $this->pxApiResponse($response,true,[],'Profile saved!',200);
        } else {
            return $this->pxApiResponse($response,false,[],'Error while saving profile',500);
        }

      } else {
        return $this->pxApiResponse($response,false,[],'User not found',404);
      }
    }

    public function updateProfile($request,$response) {
        $parsedBody = $request->getParsedBody();
        $decoded = $request->getAttribute("token");

        //check for credits
        if($user = User::find($decoded["user_id"])) {

        if(!$profile = $user->profile->where("id",$parsedBody["profile_id"])->first()) {
            return $this->pxApiResponse($response,false,[],'Profile not found',404);
        }
        
        $profile->name = $parsedBody["name"];
        $profile->image = $parsedBody["image"];
        $profile->lang = $parsedBody["lang"];


        if($profile->save()) {
            return $this->pxApiResponse($response,true,[],'Profile saved!',200);
        } else {
            return $this->pxApiResponse($response,false,[],'Error while saving profile',500);
        }

        } else {
        return $this->pxApiResponse($response,false,[],'User not found',404);
        } 
    }

    public function deleteProfile($request,$response) {
        $params = $request->getQueryParams();
        $decoded = $request->getAttribute("token");

        //check for credits
        if($user = User::find($decoded["user_id"])) {

        if(!$profile = $user->profile->where("id",$params["profile_id"])->first()) {
            return $this->pxApiResponse($response,false,[],'Profile not found',404);
        }

            try {
                $profile->favorites()->delete();
                $profile->events()->delete();
                $profile->delete();
                return $this->pxApiResponse($response,true,[],'Profile deleted!',200);

            } catch (\Throwable $th) {
                return $this->pxApiResponse($response,false,['error' => $th->getMessage()],'Error while saving profile',500);
            }

        } else {
            return $this->pxApiResponse($response,false,[],'User not found',404);
        } 
    }

    public function getProfiles($request,$response) {

        $decoded = $request->getAttribute("token");

        if($user = User::find($decoded["user_id"])) {
            return $this->pxApiResponse($response,true,$user->profile,'Profile list',200);
        } else {
            return $this->pxApiResponse($response,false,[],'User not found',404);
        }
    }

    public function getUserInfo($request,$response) {
        $decoded = $request->getAttribute("token");

        $return = array();

        if($user = User::find($decoded["user_id"])) {
            $return["devices"] = $user->device;
            $return["user"] = array(
                "name" => $user->username
                ,"credit_expires" => $user->credit_expires
            );
            return $this->pxApiResponse($response,true,$return,'Devices list',200);
        } else {
            return $this->pxApiResponse($response,false,[],'User not found',404);
        }

    }

    public function deleteDevice($request,$response) {
        $params = $request->getQueryParams();
        $decoded = $request->getAttribute("token");

        //check for credits
        if($user = User::find($decoded["user_id"])) {

        if(!$device = $user->device->where("id",$params["device_id"])->first()) {
            return $this->pxApiResponse($response,false,[],'Device not found',404);
        }

        try {
            $device->delete();
            return $this->pxApiResponse($response,true,$user->device,'Device deleted!',200);
        } catch (\Throwable $th) {
            return $this->pxApiResponse($response,false,['error' => $th->getMessage()],'Error while saving profile',500);
        }

        } else {
            return $this->pxApiResponse($response,false,[],'User not found',404);
        } 
    }

    public function getAvatars($request,$response) {
        $return["avatar"] = array();
        for ($i=1; $i < 22; $i++) { 
            $return["avatar"][] = array("id" => $i, "image" => "http://78.46.72.178/chango/img/avatares/" . $i . ".png");
        }

        return $this->pxApiResponse($response,true,$return,'Avatar list',200);
    }

    public function getDeviceToken($request,$response) {


        $params = $request->getQueryParams();
        $device = UserDevice::where("device_id",$params["device_id"])->where("enabled","1")->first();

        if ($device) {
            return $this->pxApiResponse($response,true,array(
                "token" => $device->token
            ),'Welcome to px api',200);
        } else {
            return $this->pxApiResponse($response,false,[],'Device not found',404);
        }
        
    }

    private function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function registerDevice($request,$response) {
        $parsedBody = $request->getParsedBody();

        $user = User::where("username",$parsedBody["mail"])->first();
        if($user) {
            $device = UserDevice::where("user_id",$user->id)->where("enabled",1)->get();
            if($device->count() >= $user->maxDevices) {
                return $this->pxApiResponse($response,false,[],'Alcanzaste el maximo de dispositivos conectados. Contactate con un vendedor.',401);
                //alcanzo el maximo de dispositivos
            } else {

                $deviceExistent = UserDevice::where("device_id",$parsedBody["device_id"])->first();
                if($deviceExistent) {
                    if($deviceExistent->user->id == $user->id) {
                        if($deviceExistent->enabled) {
                            //generar token y devolver de nuevo el token
                        } else {
                            //eliminar este device y generar uno nuevo
                            
                        }
                        $deviceExistent->delete();
                    } else {
                        return $this->pxApiResponse($response,false,[],'Ocurrio un error. Contactate con un vendedor.',500);
                    }
                }            

                //ok registrar el dispositivo, generar pin y enviar correo
                $pin = $this->randomNumber(6);
                $expires = new DateTime("now +18 hours");
                $userDevice = new UserDevice([
                    "device_id" => $parsedBody["device_id"],
                    "device_name" => $parsedBody["device_name"],
                    "user_id" => $user->id,
                    "pin" => $pin
                ]);

                if($userDevice->save()){
                    //send email
                    if ($this->sendEmail($user->username,'',$pin,$parsedBody["device_name"],$expires)) {
                        return $this->pxApiResponse($response,true,[],'Revisa tu casilla de correo: ' . $user->username . ' e ingresa el codigo que te enviamos a continuacion.',200);
                    } else {
                        return $this->pxApiResponse($response,false,[],'Ocurrio un error. Contactate con un vendedor.',500);
                    }

                } else {
                    //error
                    return $this->pxApiResponse($response,false,[],'Ocurrio un error. Contactate con un vendedor.',500);
                }

            }
        } else {
            return $this->pxApiResponse($response,false,[],'Necesitas comunicarte con un vendedor para empezar a disfrutar de Oneflix.',401);
        }        

    }

    public function validatePin($request,$response) {

        $parsedBody = $request->getParsedBody();
        $device = UserDevice::where("device_id",$parsedBody["device_id"])->first();

        if($device){
            if($device->pin == $parsedBody["pin"]) {
                
                $devices = UserDevice::where("user_id",$device->user->id)->where("enabled",1)->get();
                if($devices->count() >= $device->user->maxDevices - 1) {
                    return $this->pxApiResponse($response,false,[],'Alcanzaste el maximo de dispositivos conectados. Contactate con un vendedor.',401);
                }

                $profiles_user = [];
                $user = $device->user;
                $data["profiles"] = [];
                foreach ($user->profile as $value) {
                    array_push($profiles_user,$value->id);                
                    array_push($data["profiles"],array(
                        'id' => $value->id
                        ,'name' => $value->name                    
                        ,'image'=> $value->image
                        ,'lang'=> $value->lang
                    ));
                }
                //generate token
                $now = new DateTime();
                $future = new DateTime("now +365 days");
            
                $jti = (new Base62)->encode(random_bytes(16));
            
                $payload = [
                    "iat" => $now->getTimeStamp(),
                    "exp" => $future->getTimeStamp(),
                    "jti" => $jti,
                    "sub" => $user->username,
                    'user_id' => $user->id,
                    'profiles' => $profiles_user
                ];
                        
                $token = JWT::encode($payload, $_ENV["jwt_secret"], "HS256");
            
                $data["token"] = $token;
                $data["expires"] = $future->getTimeStamp();            
    
                $user->token = $token;
                $user->token_expires = $future->getTimestamp();
                $user->token_edited = $now;
                $user->token_created = $now;
                
                $user->save();


                $device->enabled = 1;
                $device->token = $token;
                $device->save();

                return $this->pxApiResponse($response,true,$data,'Bienvenido a OneFlix.',200);
                //generar token y actulizar el device


            } else {
                return $this->pxApiResponse($response,false,[],'El pin ingresado no es valido.',401);
            }
        } else {
            return $this->pxApiResponse($response,false,[],'El dispositivo ingresado no es valido.',401);
        }

    }


   private function sendEmail($mailTo,$nameTo,$pin,$name,$expires) {

    $template = $this->container->templates->loadTemplate('accessCode'); 
    $message = $template->render(array(
        "code" => $pin,
        "deviceName" => $name,
        "expires" => $expires->format("d/m/Y")
    ));

    $email = new SendGrid\Mail\Mail();
    $email->setFrom("info@cadabraonline.com", "OneFlix");
    $email->setSubject("Su codigo de acceso a OneFlix");
    $email->addTo($mailTo, $nameTo);
    $email->addContent("text/plain", "Ingrese el siguiente codigo PIN: " . $pin);
    $email->addContent(
        "text/html", $message
    );

    $sendgrid = new \SendGrid($_ENV['SENDGRID_API_KEY']);

    try {
        $sendgridStatus = $sendgrid->send($email);
        return true;

    } catch (Exception $e) {       
        return false;
    }
    
   }

   public function getAdminToken($request,$response,$args) {
    
    $auth_username = $request->getHeader('PHP_AUTH_USER')[0];
    $auth_password = $request->getHeader('PHP_AUTH_PW')[0];


    if ($user = User::where(['username' => $auth_username, "active" => 1])->first()) {
        if(password_verify($auth_password,$user->password)) {           
            //generate token
            $now = new DateTime();
            $future = new DateTime("now +1000 hours");
        
            $jti = (new Base62)->encode(random_bytes(16));
        
            $payload = [
                "iat" => $now->getTimeStamp(),
                "exp" => $future->getTimeStamp(),
                "jti" => $jti,
                "sub" => $auth_username,
                'user_id' => $user->id
            ];
                    
            $token = JWT::encode($payload, $_ENV["jwt_secret"], "HS256");
        
            $data["token"] = $token;
            $data["expires"] = $future->getTimeStamp();            

            $user->token = $token;
            $user->token_expires = $future->getTimestamp();
            $user->token_edited = $now;
            $user->token_created = $now;
            
            $user->save();

            return $response->withJson(['status' => true, 'data' => $data, 'message' => 'Welcome to PX Api']);


        } else {
            return $response->withJson(['status' => false, 'data' => [], 'message' => 'Incorrect Username / Password']);
        }
    } else {
        return $response->withJson(['status' => false, 'data' => [], 'message' => 'Incorrect Username / Password']);
    }
   }

   public function uploadFileImage($request,$response){

    $return = array();
    foreach ($_FILES["images"]["error"] as $key => $error) {

        $filename = uniqid();
        if ($finalname = $this->uploadImage($key,$error,uniqid())){
            array_push(
                $return,array(
                    "key" => $key,
                    "filename" => $_ENV["public_img_url"] . $finalname
                )
            );
        } else {
            return $this->pxApiResponse($response,false,[],'Ha ocurrido un error al subir el archivo.',500);
        }
    }
    return $this->pxApiResponse($response,true,$return,'List of uploaded files',200);

   }
}