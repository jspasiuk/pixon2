<?php

namespace App\Controllers;

use App\Models\Genre;
use App\Models\Slider;

class SliderController extends Controller
{

  public function getSliderConfig($request,$response) {

    $return = array(
      "type" => array("main","recently","common","genre"),
      "home_type" => array("all","movie","serie")
    );

    return $this->pxApiResponse($response,true,$return,'Config',200);

  }

  public function getSliders($request,$response) {

    $sliders = Slider::whereNull("deleted_at")->orderBy("order")->get();
    $sliderList = array();
    foreach ($sliders as $slider) {
      array_push($sliderList,array(
        "id" => $slider->id,
        "name" => $slider->name,
        "name_es" => $slider->name_es,
        "order" => $slider->order,
        "type" => $slider->type,
        "home_type" => $slider->home_type,
        "order_by" => $slider->order_by,
        "order_direction" => $slider->order_direction,
        "created_at" => $slider->created_at,
      ));
    }

    return $this->pxApiResponse($response,true,$sliderList,'Slider list',200);

  }

  public function getSliderById($request,$response,$args) {    

    $slider = Slider::where("id",$args["id"])->first();    

    if($slider) {

      $menu = array();
      $menu['slider'] = $slider;
      /*foreach ($slider->items as $item) {
        $content = $item->content;
      }*/
      $menu['slider']["items"] = $slider->items()->with("content:id,name")->get();
      $menu['slider']["genres"] = $slider->genres;

     return $this->pxApiResponse($response,true,$menu,'Slider items list',200);

    } else {
      return $this->pxApiResponse($response,false,[],'Carrousel not found',404);
    }

  }

  public function updateSlider($request,$response) {
    $parsedBody = $request->getParsedBody();

    $slider = Slider::find($parsedBody["id"]);

    $slider->name = $parsedBody["name"];
    $slider->name_es = $parsedBody["name_es"];
    $slider->type = $parsedBody["type"];
    $slider->order = $parsedBody["order"];
    $slider->year_from = $parsedBody["year_from"];
    $slider->year_to = $parsedBody["year_to"];
    $slider->imdb_from = $parsedBody["imdb_from"];
    $slider->imdb_to = $parsedBody["imdb_to"];

    $slider->home_type = $parsedBody["home_type"] ? $parsedBody["home_type"] : 'all';

    $slider->order_by = $parsedBody["order_by"] ? $parsedBody["order_by"] : 'RELEASED_DATE';
    $slider->order_direction = $parsedBody["order_direction"] ? $parsedBody["order_direction"] : 'DESC';

    if($slider->save()) {
      $slider->items()->delete();
      
      foreach ($slider->genres as $sliderGender) {
        $slider->genres()->detach($sliderGender->id);
       }

      foreach ($parsedBody["items"] as $item) {
        $slider->items()->create($item);
      }
      
      foreach ($parsedBody["genres"] as $genre) { 
        if(Genre::find($genre["id"])) {
          $slider->genres()->attach($genre["id"]);
        }
      }

      return $this->pxApiResponse($response,true,$slider,'Slider updated successfuly',200);
    } else {
      return $this->pxApiResponse($response,false,[],'Fatal error while saving the slider!',500);
    }

  }


  public function newSlider($request,$response) {
    $parsedBody = $request->getParsedBody();

    $slider = new Slider();
    $slider->name = $parsedBody["name"];
    $slider->name_es = $parsedBody["name_es"];
    $slider->type = $parsedBody["type"];
    $slider->order = $parsedBody["order"];
    $slider->year_from = $parsedBody["year_from"];
    $slider->year_to = $parsedBody["year_to"];
    $slider->imdb_from = $parsedBody["imdb_from"];
    $slider->imdb_to = $parsedBody["imdb_to"];
    $slider->home_type = $parsedBody["home_type"] ? $parsedBody["home_type"] : 'all';

    $slider->order_by = $parsedBody["order_by"] ? $parsedBody["order_by"] : 'RELEASED_DATE';
    $slider->order_direction = $parsedBody["order_direction"] ? $parsedBody["order_direction"] : 'DESC';

    if($slider->save()) {
      foreach ($parsedBody["items"] as $item) {
        $slider->items()->create(array(
          "content_id" => $item["content_id"],
          "order" => $item["order"]
        ));
      }
      
      foreach ($parsedBody["genres"] as $genre) { 
        if(Genre::find($genre["id"])) {
          $slider->genres()->attach($genre["id"]);
        }
      }

      return $this->pxApiResponse($response,true,$slider,'Slider created successfuly',200);
    } else {
      return $this->pxApiResponse($response,false,[],'Fatal error while saving the slider!',500);
    }

  }

  public function deleteSlider($request,$response,$args) {    

    try {
       $slider = Slider::find($args["id"]);
       $slider->items()->delete();
       foreach ($slider->genres as $sliderGender) {
        $slider->genres()->detach($sliderGender->id);
       }

       if($slider->delete()) {
          return $this->pxApiResponse($response,true,[],'Slider deleted succesfully',200);
       } else {
          return $this->pxApiResponse($response,false,[],'Error deleting slider',500);
       }
    } catch (\Throwable $th) {
       return $this->pxApiResponse($response,false,[],'Error deleting slider',500);
    }
  }

  public function deleteItem($request,$response,$args) {

    try {
       $slider = Slider::find($args["slider_id"]);

       $sliderItems = $slider->items->where("id",$args["id"]);
       foreach ($sliderItems as $item) {
          if($item->delete()) {
              return $this->pxApiResponse($response,true,[],'Slider deleted succesfully',200);
          } else {
              return $this->pxApiResponse($response,false,[],'Error deleting slider',500);
          }
       }    
    } catch (\Throwable $th) {
       return $this->pxApiResponse($response,false,[],'Error deleting slider',500);
    }
  }

}