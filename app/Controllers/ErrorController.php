<?php

namespace App\Controllers;

use App\Models\Error;

class ErrorController extends Controller
{

  public function newError($request,$response) {
    $parsedBody = $request->getParsedBody();
    $profile_id = $request->getHeader('profileId')[0];      

    $error = new Error();
    $error->content_id = $parsedBody["content_id"];
    $error->video_id = $parsedBody["video_id"];
    $error->profile_id = $profile_id;

    $error->motive = $parsedBody["motive"];
    $error->comment = $parsedBody["comment"];
   
    if($error->save()) {
      return $this->pxApiResponse($response,true,$error,'Gracias por reportar el error. Lo revisaremos cuanto antes.',200);
    } else {
      return $this->pxApiResponse($response,false,[],'Ha ocurrido un error al grabar el reporte. Por favor intente nuevamente mas tarde.',500);
    }

  }

  public function show($request,$response) {

    $errors = Error::with('video.content','profile.user')->has('video.content')->get();
    return $this->pxApiResponse($response,true,$errors,'Error list.',200);

  }

  public function delete($request,$response,$args) {

    $error = Error::find($args['id']);
    if($error->delete()) {
      return $this->pxApiResponse($response,true,$error,'Error deleted.',200);
    } else {
      return $this->pxApiResponse($response,false,[],'Error not deleted.',500);
    }

  }


}