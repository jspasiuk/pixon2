<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use App\Models\User;
use App\Models\Slider;
use App\Models\Content;
use App\Models\Genre;
use App\Models\Company;
use App\Models\ContentVideo;
use App\Models\ContentActor;
use App\Models\Event;
use App\Models\Actor;
use App\Models\Profile;
use App\Models\Proxy;
use App\Models\ProfileFavorites;
use App\Models\ContentBackdrop;
use App\Models\ContentPoster;
use App\Models\ContentCompany;
use App\Models\ContentGenre;
use App\Models\ContentSimilar;
use App\Models\ContentProductionCountry;
use App\Models\ContentRecommendation;
use anlutro\cURL\cURL as CURL;
use App\Models\ChronProcessList;
use App\Models\ChronProcessStep;
use App\Models\Country;
use App\Models\SliderItem;
use App\Models\EncodingServer;
use Illuminate\Database\Eloquent\Builder;
use stdClass;

class ContentController extends Controller
{


   public function __construct(ContainerInterface $container)
   {
       $this->container = $container;
   }

   private function getMenuInner($home_type,$lang,$atv,$profile_id) {
      
      $return = array();
      $sliders = Slider::where("home_type",$home_type)->whereNull("deleted_at")->orderBy("order","asc")->limit(20)->get();
      

      //get continue watching for users
      $events = Event::where("profile_id",$profile_id)->where("type","watch")->limit(15)->groupBy('content_id')->orderBy("updated_at","desc");
      $events = $events->whereHas('content',function (Builder $query) use ($home_type){
         $query->whereNull('deleted_at');
         if($home_type == "movie" || $home_type == "serie") {
            $query->where("type",$home_type);
         }
      })->limit(20)->get();

      if($events->count()) {
         $appendContinue = array(
            "name" => $lang === 'es' ? 'Seguir mirando' : 'Continue Watching',
            "type" => 'continue',
            "order" => 0,
            "items" => array()
         );
   
         $appendContinue["items"] = $this->getCommonSlider($events,$lang,true,$atv);
   
   
         if($appendContinue["items"]){
            array_push($return,$appendContinue);
         }
      }

      foreach ($sliders as $slider) {
         
         $append = array(
            "name" => $lang === 'es' ? $slider->name_es : $slider->name,
            "type" => $slider->type,
            "order" => $slider->order,
            "items" => array()
         );
         $items = $slider->items()->whereHas('content.videos',function (Builder $query) {
            $query->whereNotNull('published_date');
         })->orderBy("order","asc")->limit(20)->get();

         if($items->count()) {
            $append["items"] = $this->getCommonSlider($items,$lang,false,$atv);
         } else {
            switch ($home_type) {
               case 'movie':
               case 'serie':
                  $contents = Content::where("type",$home_type)->whereNull("deleted_at");
                  break;
               default:
                  $contents = Content::whereNull("deleted_at");
                  break;
            }

            $contents = $contents->select("content.*");
            $contents = $contents->join("content_video",'content_video.content_id','=','content.id');
            $contents = $contents->whereNotNull("content_video.published_date");

            $contents = $contents->groupBy("content.id");

            if($slider->year_from) {
               $contents = $contents->whereRaw(" year(release_date) >= ?", [$slider->year_from]);
            }
            if($slider->year_to) {
               $contents = $contents->whereRaw(" year(release_date) <= ?", [$slider->year_to]);
            }
            if($slider->imdb_from) {
               $contents = $contents->where('imdb',">=",$slider->imdb_from);
            }
            if($slider->imdb_to) {
               $contents = $contents->where('imdb',"<=",$slider->imdb_to);
            }

            if($slider->genres->count()) {
               $findGenres = array();
               foreach ($slider->genres as $genre) {
                  array_push($findGenres,$genre->id);
               }
               $contents = $contents->whereHas('genres',function (Builder $query) use($findGenres) {
                  $query->where('genre.id',$findGenres);
               });
            }

            //$contents = $contents->orderBy('videos.published_date','desc');

            switch ($slider->order_by) {
               case 'RELEASED_DATE':
                  if($slider->order_direction === 'DESC') {
                     $contents = $contents->orderBy('release_date','desc');
                  } else if($slider->order_direction === 'ASC') {
                     $contents = $contents->orderBy('release_date','asc');
                  } else {
                     $contents = $contents->inRandomOrder();
                  }
                  break;
               case 'PUBLISHED_DATE':
                  if($slider->order_direction === 'DESC') {
                     $contents = $contents->orderBy('content_video.published_date','desc');
                  } else if($slider->order_direction === 'ASC') {
                     $contents = $contents->orderBy('content_video.published_date','asc');
                  } else {
                     $contents = $contents->inRandomOrder();
                  }
                  break;
               default:
                  $contents = $contents->inRandomOrder();
                  break;
            }
            $contents = $contents->limit(20)->get();
            $items = $this->simulateItem($contents);
            if($items) {
               $append["items"] = $this->getCommonSlider($items,$lang,false,$atv);
            } else {
               continue;
            }

         }

         array_push($return,$append);
      }

      return $return;
   }

   public function getMenu($request,$response,$args) {

      $profile_id = $request->getAttribute('profileId');
      $params = $request->getQueryParams();
      $profile = Profile::find($profile_id);
      $lang = $profile->lang;

      if($args["home_type"] === 'complete') {
         $return["all"] = $this->getMenuInner('all',$lang,$params["atv"],$profile_id);
         $return["movie"] = $this->getMenuInner('movie',$lang,$params["atv"],$profile_id);
         $return["serie"] = $this->getMenuInner('serie',$lang,$params["atv"],$profile_id);

      } else {
         $return = $this->getMenuInner($args["home_type"],$lang,$params["atv"],$profile_id);
      }

      return $this->pxApiResponse($response,true,$return,'ok',200);
   }

   private function simulateItem($contents) {
      $items = array();
      $cont=0;
      foreach ($contents as $content) {
         $cont++;
         $item = new stdClass();
         $item->content = $content;
         $item->id = $cont;
         $item->order = $cont;
         array_push($items,$item);
      }
      return $items;
   }

   private function getTranslatedContent(Content $content,$lang = "es") {
      $switch = array(
         "name" => "",
         "tagline" => "",
         "plot" => "",
         "poster" => "",
         "backdrop" => $content->backdrops()->inRandomOrder()->first()->url
      );
      $posterEs = $content->posters->where("lang",'es');
      $posterEn = $content->posters->where("lang",'en');

      $logoEn = $content->logos->where("lang",'en');
      $logoEs = $content->logos->where("lang",'es');
      $logo = null;
      switch ($lang) {
         case 'es':
            if($logoEs->count()) {
               $logo = $logoEs->shuffle()->first()->url;
            } else if ($logoEn->count()) {
               $logo = $logoEn->shuffle()->first()->url;
            } else {
               $logo = null;
            }
            $switch["name"] = $content->name_es ? $content->name_es : $content->name;
            $switch["tagline"] = $content->tagline_es ? $content->tagline_es : $content->tagline;
            $switch["plot"] = $content->plot_es ? $content->plot_es : $content->plot;
            $switch["poster"] = $posterEs->count() ? $posterEs->shuffle()->first()->url : $posterEn->shuffle()->first()->url;
            $switch["logo"] = $logo;
            break;
         case 'en':
            if($logoEn->count()) {
               $logo = $logoEn->shuffle()->first()->url;
            } else if ($logoEs->count()) {
               $logo = $logoEs->shuffle()->first()->url;
            } else {
               $logo = null;
            }
            $switch["name"] = $content->name ? $content->name : $content->name_es;
            $switch["tagline"] = $content->tagline ? $content->tagline : $content->tagline_es;
            $switch["plot"] = $content->plot ? $content->plot : $content->plot_es;
            $switch["poster"] = $posterEn->count() ? $posterEn->shuffle()->first()->url : $posterEs->shuffle()->first()->url;
            $switch["logo"] = $logo;
            break;
      }
      return $switch;
   }

   private function getCommonSlider($items,$lang = 'es',$event = false,$atv = false) {
      $slider = array();
      foreach ($items as $item) {
         if($item->content->videos->count()) {
            $translation = $this->getTranslatedContent($item->content,$lang);

            $localitem = array();
            $localitem["id"] = $item->id;
            $localitem["order"] = $item->order;
            $localitem["content"]["id"] = $item->content->id;
            $localitem["content"]["poster"] = $translation["poster"];
            $localitem["content"]["logo"] = $translation["logo"];
            $localitem["content"]["backdrop"] = $item->content->backdrops()->inRandomOrder()->first()->url;
            $localitem["content"]["type"] = $item->content->type;
            $localitem["content"]["name"] = $translation["name"];
            if($event) {
               $localitem["event"]["video_id"] = $item->content_video_id;
               $localitem["event"]["elapsed"] = $item->time;
               $localitem["video"]["duration"] = $item->video->duration;
               $localitem["content"]["video"]["id"] = $item->content_video_id;
               $localitem["content"]["video"]["qualitys"] = $item->video->qualitys;
            } else {
               $localitem["content"]["video"]["id"] = $item->content->videos()->first()->id;
               $localitem["content"]["video"]["duration"] = $item->content->videos()->first()->duration;
               $localitem["content"]["video"]["qualitys"] = $item->content->videos()->first()->qualitys;
            }
            if($atv) {
               $localitem["content"]["trailer_id"] = $item->content->trailer_id;
               $localitem["content"]["plot"] = $translation["plot"];
               $localitem["content"]["release_date"] = $item->content->release_date;
               $localitem["content"]["imdb"] = $item->content->imdb;
               if($item->content->type === 'movie') {
                  $localitem["video"]["duration"] = $item->content->videos()->first()->duration;
               } else {
                  $localitem["content"]["seasons"] = $item->content->videos()->select("season")->groupBy("season")->whereNotNull("published_date")->get()->count();
               }
            }
            array_push($slider,$localitem);
         }
      }
      return $slider;
   }

   public function getFavorites($request,$response) {
      
      $profile_id = $request->getHeader('profileId')[0];
      $profile = Profile::find($profile_id);
      $params = $request->getQueryParams();
      
      $return["items"] = array();
      foreach ($profile->favorites as $favorites) {
         $item["content"] = array();
         $translation = $this->getTranslatedContent($favorites->content,$profile->lang);
         if($params["atv"]) {
            $item["content"] = array(
               'id' => $favorites->content->id,
               'type' => $favorites->content->type,
               "trailer_id" => $favorites->content->trailer_id,
               'name' => $translation["name"],
               'imdb' => $favorites->content->imdb,
               'release_date' => $favorites->content->release_date,
               'poster' => $translation["poster"],
               'backdrop' => $translation["backdrop"],
               'plot' => $translation["plot"],
               'seasons' => $favorites->content->type === 'serie' ? $favorites->content->videos()->select("season")->groupBy("season")->whereNotNull("published_date")->get()->count() : null ,
            );
            $item["content"]["video"]["duration"] = $favorites->content->type === 'movie' ? $favorites->content->videos()->first()->duration : 0;
            $item["content"]["video"]["id"] = $favorites->content->videos()->first()->id;
         } else {
            $item["content"] = array(
               'id' => $favorites->content->id,
               'type' => $favorites->content->type,
               'name' => $translation["name"],
               'poster' => $translation["poster"]
            );
         }
         array_push($return["items"],$item);
      }

      return $this->pxApiResponse($response,true,$return,'ok',200);

   }

   public function deleteContentToProfile ($request,$response) {

      $params = $request->getQueryParams();
      $profile_id = $request->getHeader('profileId')[0];

      $favorite = ProfileFavorites::where("content_id",$params["content_id"])->where("profile_id",$profile_id)->delete();

      return $this->pxApiResponse($response,true,$params,'Se quito el contenido de tu lista.',200);

   }

   public function saveContentToProfile($request,$response) {

      $parsedBody = $request->getParsedBody();
      $profile_id = $request->getHeader('profileId')[0];

      if ($favorite = ProfileFavorites::updateOrCreate([
         "profile_id" => $profile_id,
         "content_id" => $parsedBody["content_id"]
      ])) {   
         return $this->pxApiResponse($response,true,[],'Se agrego el contenido a tu lista.',200);
      } else {
         return $this->pxApiResponse($response,false,[],'Ha ocurrido un error al grabar la lista.',500);
      }      

   }

   public function searchContent($request,$response) {

      if ($profile_id = $request->getHeader('profileId')[0]) {
         $profile = Profile::find($profile_id);
         $lang = $profile->lang;
      } else {
         $lang = "en";
      }
      
      $params = $request->getQueryParams();

      $contents = Content::whereHas('videos',function (Builder $query){
         $query->whereNotNull('published_date');
      })->whereNull("deleted_at");

      if($params["name"]) {
         $contents = $contents->where(function($query) use ($params) {
            $query->where("name",'like','%' . $params["name"] . '%')
                  ->orWhere("name_es",'like','%' . $params["name"] . '%');
         })->limit(200)->get();
      } else {
         if($params["type"]) {
            $contents = $contents->where("type",$params["type"])->limit(200)->orderBy("release_date")->get();
         }
         if($params["actor"]) {
            $contents = $contents->whereHas('actors',function (Builder $query) use($params) {
               $query->where('actor.id',$params["actor"]);
            })->get();
         }
      }

      if($contents->count() > 0) {
         $return["items"] = array();
         foreach ($contents as $content) {
            $translation = $this->getTranslatedContent($content,$lang);
            $item["content"] = array();
            if($content->posters) {
               if($params["atv"]) {
                  $item["content"] = array(
                     'id' => $content->id,
                     'type' => $content->type,
                     'name' => $translation["name"],
                     'imdb' => $content->imdb,
                     'release_date' => $content->release_date,
                     "trailer_id" => $content->trailer_id,
                     'poster' => $translation["poster"],
                     'backdrop' => $translation["backdrop"],
                     'plot' => $translation["plot"],
                     'seasons' => $content->type === 'serie' ? $content->videos()->select("season")->groupBy("season")->whereNotNull("published_date")->get()->count() : null ,
                  );
                  $item["content"]["video"]["id"] = $content->videos()->first()->id;
                  $item["content"]["video"]["duration"] = $content->type === 'movie' ? $content->videos()->first()->duration : 0;
               } else {
                  $item["content"] = array(
                     'id' => $content->id,
                     'type' => $content->type,
                     'name' => $translation["name"],
                     'poster' => $translation["poster"]
                  );
               }
               array_push($return["items"],$item);
            }
         }

         return $this->pxApiResponse($response,true,$return,'ok',200);

      } else {
         return $this->pxApiResponse($response,true,[],'Content not found!',200);
      }
   }

   public function playContent($request,$response,$args) {

      $profile_id = $request->getHeader('profileId')[0];
      $decoded = $request->getAttribute("token");

      //check for credits
      if($user = User::find($decoded["user_id"])) {
         $credits = $user->credit_expires;
         if($credits) {

            if($credits > date("Y-m-d H:i:s")) {
               //todo ok, continue

            } else {
               return $this->pxApiResponse($response,false,[],'Lo lamentamos, pero no tiene creditos para reproducir el contenido.',402);
            }

         } else {
            //no credits
            return $this->pxApiResponse($response,false,[],'Lo lamentamos, pero necesita creditos para reproducir el contenido.',402);
         }
      } else {
         return $this->pxApiResponse($response,false,[],'User not found!',404);
      }

      if ($video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first()){

         //Save the event for the content video id and the profile_id
         $event = new Event();
         $event->content_id = $video->content_id;
         $event->content_video_id = $video->id;
         $event->profile_id = $profile_id;
         $event->time = 0;
         $event->type = 'watch';

         //$event->save();

         //return mpd file
         $qualityes = array();
         foreach ($video->quality as $quality) {
            array_push($qualityes,array("id" => $quality->id,"name" => $quality->name, "url" => $video->server->address . '/' .  $video->folder . '/' . $quality->filename));
         }

         return $this->pxApiResponse($response,true,array(
            'url' => $video->server->address . '/' . $video->folder . '/' . $video->filename,
            'duration' => $video->duration,
            'quality' => $qualityes
         ),'ok',200);

      } else {
         return $this->pxApiResponse($response,false,[],'Video not found!',404);
      }      

   }

   public function updateEventContent($request,$response) {
      $parsedBody = $request->getParsedBody();
      $profile_id = $request->getHeader('profileId')[0];


      $event = Event::updateOrCreate(
         ["content_id" => $parsedBody["content_id"], "content_video_id" => $parsedBody["video_id"], "profile_id" => $profile_id, "type" => "watch" ],
         ["time" => intval($parsedBody["time"])]
      );

      return $this->pxApiResponse($response,true,$event,'ok',200);

   }

   private function getCommonActor($actors) {
      $return = array();
      foreach ($actors as $actor) {
         array_push($return,array(
            'id' => $actor->id,
            'name' => $actor->name,
            'job' => $actor->pivot->job,
            'image' => $actor->image
         ));
      }
      return $return;
   }

   private function getCommonContent(Content $content)
   {
      
      $content['id'] = $content->id;
      $content['type'] = $content->type;
      $content['name'] = $content->name;
      $content['name_es'] = $content->name_es;
      $content['tagline_'] = $content->tagline;
      $content['tagline_es'] = $content->tagline_es;
      $content['release_date'] = $content->release_date;
      $content['plot'] = $content->plot;
      $content['plot_es'] = $content->plot_es;
      $content['genres'] = $content->genres;
      $content['actors'] = $this->getCommonActor($content->actors);
      $content['poster'] = $content->posters;
      $content['backdrop'] = $content->backdrops;
      $content['companies'] = $content->companies;
      $content['productionCountries'] = $content->productionCountries;
      $content['trailer'] = $content->trailer_id;
      $content['imdb'] = $content->imdb;
      $content['lang'] = $content->lang;

      return $content;
   }

   private function getLastContentLog($profile_id,$content_id,$video_id = null) {

      $event = Event::where("profile_id",$profile_id)->where("content_id",$content_id);
      if($video_id) {
         $event = $event->where("content_video_id",$video_id);
      }
      $event = $event->latest('updated_at')->first();
      if ($event) {
         return array(
            'id' => $event->id,
            'content_id' => $event->content_id,
            'video_id' => $event->content_video_id,
            'time' => $event->time
         );
      } else {
         return null;
      }
   }

   public function newContent($request,$response) {
      $parsedBody = $request->getParsedBody();

      $content = new Content();
      $content->name = $parsedBody["name"];
      $content->name_es = $parsedBody["name_es"];
      $content->tagline = $parsedBody["name_es"];
      $content->tagline_es = $parsedBody["tagline_es"];
      $content->type = $parsedBody["type"];
      $content->source = $parsedBody["source"];
      $content->plot = $parsedBody["plot"];
      $content->plot_es = $parsedBody["plot_es"];      
      $content->release_date = $parsedBody["release_date"];
      $content->trailer_id = $parsedBody["trailer_id"];
      $content->imdb = $parsedBody["imdb"];
      $content->tmdb_id = $parsedBody["tmdb_id"];
      $content->imdb_id = $parsedBody["imdb_id"];

      $videoController = new ContentVideoController();

      if ($content->save()) {
         foreach ($parsedBody["videos"] as $video) {
            //chequear y meter error en un array de estados
            $videoController->createContentVideo($content,$video);
         }

         foreach ($parsedBody["actors"] as $actorContent) {             
            if($actor = Actor::find($actorContent["id"])) {
               $content->actors()->attach($actor->id, ['job' => $actorContent["job"]]);
            }
         }

         foreach ($parsedBody["posters"] as $poster) {             
            $content->posters()->create(array(
               "url" => $poster["url"],
               "lang" => $poster["lang"]
            ));
         }

         foreach ($parsedBody["backdrops"] as $backdrop) {             
            $content->backdrops()->create(array(
               "url" => $backdrop["url"]
            ));
         }

         foreach ($parsedBody["companies"] as $company) {             
            if ($asoc = Company::find($company["id"])) {
               $content->companies()->save($asoc);
            }
         }

         foreach ($parsedBody["genres"] as $genre) {
            if ($asoc = Genre::find($genre["id"])) {
               $content->genres()->save($asoc);
            }
         }

         foreach ($parsedBody["production_countries"] as $country) {
            if ($asoc = Country::where('code',$country["code"])->first()) {
               $content->productionCountries()->save($asoc);
            }
         }

         return $this->pxApiResponse($response,true,$this->getCommonContent($content),'Content created succesfully',200);
      } else {
         return $this->pxApiResponse($response,false,[],'Error saving content',500);
      }
   }

   public function deleteProfileEvent($request,$response,$args) {
      try {
         $profile_id = $request->getHeader('profileId')[0];

         Event::where("profile_id",$profile_id)->where("content_id",$args["content_id"])->delete();
         return $this->pxApiResponse($response,true,[],'Success',200);

      } catch (\Throwable $th) {
         return $this->pxApiResponse($response,false,[],'Error deleting event',500);
      }
   }

   public function deleteContent($request,$response,$args) {
      try {

         $status = array(
            'deleteCallUrl' => array(),
            'deleteCallResponse' => array()
         );

         $curl = new CURL;
         $content = Content::find($args["id"]);
         Event::where("content_id",$content->id)->delete();

         ContentActor::where("content_id",$content->id)->delete();
         ProfileFavorites::where("content_id",$content->id)->delete();
         ContentBackdrop::where("content_id",$content->id)->delete();
         ContentPoster::where("content_id",$content->id)->delete();
         ContentGenre::where("content_id",$content->id)->delete();
         ContentCompany::where("content_id",$content->id)->delete();
         ContentProductionCountry::where("content_id",$content->id)->delete();
         ContentSimilar::where("content_id",$content->id)->delete();
         ContentRecommendation::where("content_id",$content->id)->delete();
         SliderItem::where("content_id",$content->id)->delete();

         foreach ($content->videos as $video) {

            $video->qualitys()->delete();
            $video->subtitles()->delete();
            $video->audios()->delete();

            $folder = $video->folder;

            if($video->delete()) {
               
               $generatedHash = md5($folder . date('d-m-Y') . '-G(e@^&p93HPg(3<t');
               $call = $_ENV["SERVER_PROD_FILES"] . "files.php?path=".base64_encode($folder)."&hash=".base64_encode($generatedHash);

               $deleteStatus = $curl->get($call);
               $deleteStatusDecode = json_decode($deleteStatus,true);  

               $deleteStatusDecode["url"] = $call;
               array_push($status["deleteCallResponse"],$deleteStatusDecode);

            } else {

            }
         }


         if($content->delete()) {

            return $this->pxApiResponse($response,true,$status,'Content deleted succesfully',200);
         } else {
            return $this->pxApiResponse($response,false,[],'Error deleting content',500);
         }
      } catch (\Throwable $th) {
         return $this->pxApiResponse($response,false,[],'Error deleting content',500);
      }      

   }


   public function getContentFilteredList($request,$response) {

         $LIMIT = 10;
         $params = $request->getQueryParams();
         $returnList = [];

         $contents = Content::whereHas('videos',function (Builder $query){
            $query->whereNotNull('published_date');
         })->orderBy("id","desc")->limit($LIMIT);

         if($params["cursor"]) {
            $contents = $contents->where('id','<',$params["cursor"]);
         }

         $params["tmdbId"] ? $contents = $contents->where("tmdb_id",$params["tmdbId"]) : null;
         $params["name"] ? $contents = $contents->where('name','like','%'.$params["name"].'%') : null;
         $params["type"] ? $contents = $contents->where('type',$params["type"]) : null;

         //FILTROS
         $contents = $contents->get();

         foreach ($contents as $content) {

            $poster = $content->has('posters') ? $content->posters()->first()->url : null;

            array_push($returnList,array(
                  "id" => $content->id,
                  "name" => $content->name,
                  "tmdb_id" => $content->tmdb_id,
                  "type" => $content->type,
                  "poster_url" => $poster,
                  "videos" => $content->videos()->whereNotNull('published_date')->count(),
                  "release_date" => $content->release_date,
                  "created_at" => $content->created_at
               ));
         }

         $contents->count() >= $LIMIT ? $cursor = $content->id : $cursor = null;

         $return = array("list" => $returnList, "cursor" => $cursor);

         return $this->pxApiResponse($response,true,$return,'list',200);
   }


   public function getContentList($request,$response,$args) {

      $returnList = array();

      switch ($args["status"]) {
         case 'published':
            $contents = Content::whereHas('videos',function (Builder $query){
               $query->whereNotNull('published_date');
            })->get();
            foreach ($contents as $content) {

               $poster = $content->has('posters') ? $content->posters()->inRandomOrder()->first()->url : null;

               array_push($returnList,array(
                     "id" => $content->id,
                     "name" => $content->name,
                     "type" => $content->type,
                     "poster_url" => $poster,
                     "release_date" => $content->release_date,
                     "created_at" => $content->created_at
                  ));
            }
            break;
         case 'pending':

            $videos = ContentVideo::whereNull("published_date")->get();
            foreach ($videos as $video) {        
                      
               $video->qualitys;
               $video->audios;
               $video->subtitles;
               $video->server;

               array_push($returnList,array(
                  "id" => $video->content->id,
                  "name" => $video->content->name,
                  "video" => $video
                  ));
            }
            break;
         case 'pending-files':

            $curl = new CURL;
            $returnList = array();

            $servers = EncodingServer::where("status",1)->get();

            foreach ($servers as $server) {

               try {
                  //$curl = $curl->setOption(CURLOPT_TIMEOUT, 3000);
                  $files = $curl->get($server->url . '?p=&p=Pe}`>qpEcDftVJs(}+XVH+rFv"jwXVvxJE2Xfe6aUP}89=;Hd#PNhsj3fsbeL-pVj`KwbX7bLZ&c(MhgK@/_=u]cj%S-A)EYv7bB?[{#LucJ!2v?vP]u#<tPk7&r%ua');
                  $files = json_decode($files,true);
   
                  foreach ($files as $file) {
                  
                     $cuteFile = end(explode("/",$file["name"]));
      
                     $chronList = ChronProcessList::where("filename",$cuteFile)->orderBy('id', 'desc')->first();
                     if($chronList) {
                        if($chronList->status == 'doing' || $chronList->status== 'pending') {
                           //no lo mostramos para que lo vuelva a enviar a procesar.
                           continue;
                        }
                     }               
      
                     $fileName = pathinfo($file["name"], PATHINFO_FILENAME); // removes file extension             
                     $fileParts = explode('-',$fileName);
                     $tmdbID = $fileParts[1];
      
                     if($tmdbID) {
                        $content = Content::select("id","name")->where("tmdb_id",$tmdbID)->whereNull("deleted_at")->first();

                        $video = ContentVideo::where("download_file",$cuteFile)->whereNotNull("published_date")->first();

                        array_push($returnList ,array(
                           "name" => $cuteFile,
                           "duration" => $file["duration"],
                           "streams" => $file["streams"],
                           "content" => array(
                              "id" => $content->id,
                              "name" => $content->name
                           ),
                           "server_id" => $server->id,
                           "duplicated" => $video ? true : false
                        ));
                     } else {
                        array_push($returnList ,array(
                           "name" => $cuteFile,
                           "duration" => $file["duration"],
                           "streams" => $file["streams"],
                           "content" => null,
                           "server_id" => $server->id,
                        ));
                     }
      
                  }
               } catch (\Throwable $th) {
                  //throw $th;
                  array_push($returnList ,array(
                     "name" => $th->getMessage(),
                     "duration" => null,
                     "streams" => null,
                     "content" => null,
                     "server_id" => $server->id
                  ));
                  
               }
   
            }

            break;
         case 'process-queue':
               $returnList = array();
               $doings = ChronProcessList::whereIn("status",["pending","doing"])->orderBy("current_step","desc")->get();
               foreach ($doings as $doing) {                                 
                  array_push($returnList,array(
                     "id" => $doing->id,
                     "server_id" => $doing->server_id,
                     "server" => [
                        "id" => $doing->server->id,
                        "name" => $doing->server->name,
                        "status" => $doing->server->status ? $doing->server->status : null,
                     ],
                     "filename" => $doing->filename,
                     "step" => $doing->current_step,
                     "step_name" => $doing->step->name,
                     "step_desc" => $doing->step->desc,
                     "status" => $doing->status,
                     "content" => array(
                        "id" => $doing->content->id,
                        "name" => $doing->content->name,
                     ),
                     "percentage" => $this->getPercentageFile($doing->current_step),
                     "created_at" => $doing->created_at,
                     "updated_at" => $doing->updated_at                     
                  ));
               }

            break;
         default:
            return $this->pxApiResponse($response,true,[],'Expected published - pending - pending-files - process-queue',401);  
            break;
      }


      return $this->pxApiResponse($response,true,$returnList,'list',200);

   }

   public function getEncodingServers($request,$response,$args) {

      $servers = EncodingServer::get();

      return $this->pxApiResponse($response,true,$servers,'list',200);

   }

   public function deleteFile($request,$response,$args) {
      $params = $request->getQueryParams();

      $curl = new CURL;

      $server = EncodingServer::find($params["server_id"]);
      if(!$server) {
         return $this->pxApiResponse($response,false,[],'Server not found.',500);
      }

      //todo get content into list
      $url = $server->url_delete . "?filename=" . $params["filename"] . '&p=&p=Pe}`>qpEcDftVJs(}+XVH+rFv"jwXVvxJE2Xfe6aUP}89=;Hd#PNhsj3fsbeL-pVj`KwbX7bLZ&c(MhgK@/_=u]cj%S-A)EYv7bB?[{#LucJ!2v?vP]u#<tPk7&r%ua';
      $status = $curl->get($url);
      $status = json_decode($status,true);
      
      if($status["status"]) {
         return $this->pxApiResponse($response,true,[],'File deleted OK',200);
      } else {
         return $this->pxApiResponse($response,false,[],'Cannot delete file, check server status.',500);
      }
      

   }

   public function deleteProcessList($request,$response,$args) {
      if(ChronProcessList::where("id",$args["id"])->delete()) {
         return $this->pxApiResponse($response,true,[],'Content removed from process successfuly',200);
      } else {
         return $this->pxApiResponse($response,false,[],'Error while removing content',500);
      }
   }

   public function saveProcessList($request,$response) {
      $parsedBody = $request->getParsedBody();

      foreach ($parsedBody["list"] as $list) {
         $chronList = new ChronProcessList();
         $chronList->server_id = $list["server_id"];
         $chronList->filename = $list["filename"];
         $chronList->duration = $list["duration"];
         $chronList->content_id = $list["content_id"];         
         $chronList->sn_1080 = $list["sn_1080"];
         $chronList->sn_720 = $list["sn_720"];
         $chronList->sn_360 = $list["sn_360"];
         $chronList->audio_en = $list["audio_en"];
         $chronList->audio_es = $list["audio_es"];
         $chronList->audio_lat = $list["audio_lat"];
         $chronList->sub_es = $list["sub_es"];
         $chronList->sub_en = $list["sub_en"];
         $chronList->status = "pending";
         $chronList->current_step = 0;

         if(!$chronList->save()) {
            return $this->pxApiResponse($response,false,[],'Error while saving filename: ' . $list["filename"],500);
         }
      }   
      
      return $this->pxApiResponse($response,true,[],'List added to process successfuly',200);
   }

   private function getPercentageFile($step) {
      return intval(ChronProcessStep::where("step","<",$step)->sum("percentage"));
   }

   public function deleteCountry($request,$response,$args) {

      //$find = Content::find($args["content_id"])->productionCountries->where("id",$args["id"])->first()->delete();
      $find = Content::find($args["content_id"])->productionCountries()->detach($args["code"]);

      return $this->pxApiResponse($response,true,[],'Country deleted',200);
   
   }

   public function createCountry($request,$response,$args) {

      $parsedBody = $request->getParsedBody();
      $content = Content::find($args["content_id"]);

      if ($asoc = Country::where("code",$parsedBody["code"])->first()) {
         $content->productionCountries()->save($asoc);
      }
   
      return $this->pxApiResponse($response,true,[],'Country added',200);
   
   }

   public function createCompany($request,$response,$args) {

      $parsedBody = $request->getParsedBody();
      $find = Content::find($args["content_id"]);

      if ($asoc = Company::find($parsedBody["id"])) {
         $find->companies()->save($asoc);
      }
   
      return $this->pxApiResponse($response,true,[],'Company added',200);
   
   }

   public function deleteCompany($request,$response,$args) {

      $find = Content::find($args["content_id"])->companies()->detach($args["company_id"]);
   
      return $this->pxApiResponse($response,true,[],'Company deleted',200);
   
   }

   public function deleteLogo($request,$response,$args) {

      $find = Content::find($args["content_id"])->logos->where("id",$args["id"])->first()->delete();
   
      return $this->pxApiResponse($response,true,[],'Logo deleted',200);
   
   }

   public function deletePoster($request,$response,$args) {

      $find = Content::find($args["content_id"])->posters->where("id",$args["id"])->first()->delete();
   
      return $this->pxApiResponse($response,true,[],'Poster deleted',200);
   
   }

   public function createPoster($request,$response,$args) {

      $parsedBody = $request->getParsedBody();
      $content = Content::find($args["content_id"]);

      $poster = $content->posters()->create(array(
         "url" => $parsedBody["url"],
         "lang" => $parsedBody["lang"],
      ));
   
      return $this->pxApiResponse($response,true,$poster,'poster added',200);
   
   }


   public function deleteBackdrop($request,$response,$args) {

      $find = Content::find($args["content_id"])->backdrops->where("id",$args["id"])->first()->delete();
   
      return $this->pxApiResponse($response,true,[],'Backdrop deleted',200);
   
   }

   public function createBackdrop($request,$response,$args) {

      $parsedBody = $request->getParsedBody();
      $content = Content::find($args["content_id"]);

      $backdrop = $content->backdrops()->create(array(
         "url" => $parsedBody["url"]
      ));
   
      return $this->pxApiResponse($response,true,$backdrop,'backdrop added',200);
   
   }

   public function createGenre($request,$response,$args) {

      $parsedBody = $request->getParsedBody();
      $find = Content::find($args["content_id"]);

      if ($asoc = Genre::find($parsedBody["id"])) {
         $find->genres()->save($asoc);
      }
   
      return $this->pxApiResponse($response,true,[],'Genre added',200);
   
   }

   public function deleteGenre($request,$response,$args) {

      $find = Content::find($args["content_id"])->genres()->detach($args["genre_id"]);
   
      return $this->pxApiResponse($response,true,[],'Genre deleted',200);
   
   }

   public function getCountries($request,$response) {
      $countries = Country::get();
      return $this->pxApiResponse($response,true,$countries,'Countries list',200);
   }

   public function getGenre($request,$response,$args) {
      
      $categories = Genre::get();
      return $this->pxApiResponse($response,true,$categories,'Genre list',200);

   }

   public function getCompany($request,$response,$args) {
      
      $categories = Company::get();
      return $this->pxApiResponse($response,true,$categories,'company list',200);

   }

   public function createActor($request,$response,$args) {

      $parsedBody = $request->getParsedBody();
      $content = Content::find($args["content_id"]);

      if($actor = Actor::find($parsedBody["id"])) {
         $content->actors()->attach($actor->id, ['job' => $parsedBody["job"]]);
      }
   
      return $this->pxApiResponse($response,true,[],'Actor added',200);
   
   }

   public function deleteActor($request,$response,$args) {

      $find = Content::find($args["content_id"])->actors()->detach($args["actor_id"]);
   
      return $this->pxApiResponse($response,true,[],'Actor deleted',200);
   
   }

   public function getActor($request,$response,$args) {
      
      $categories = Actor::get();
      return $this->pxApiResponse($response,true,$categories,'actor list',200);

   }

   public function getProxy($request,$response,$args) {
      
      $categories = Proxy::get();
      return $this->pxApiResponse($response,true,$categories,'proxy list',200);

   }

   public function updateContent($request,$response,$args) {

      $content = Content::find($args["id"]);
      $parsedBody = $request->getParsedBody();

      $content->name = $parsedBody["name"];
      $content->name_es = $parsedBody["name_es"];
      $content->tagline = $parsedBody["name_es"];
      $content->tagline_es = $parsedBody["tagline_es"];
      $content->type = $parsedBody["type"];
      $content->source = $parsedBody["source"];
      $content->plot = $parsedBody["plot"];
      $content->plot_es = $parsedBody["plot_es"];      
      $content->release_date = $parsedBody["release_date"];
      $content->trailer_id = $parsedBody["trailer_id"];
      $content->imdb = $parsedBody["imdb"];
      $content->tmdb_id = $parsedBody["tmdb_id"];
      $content->imdb_id = $parsedBody["imdb_id"];

      $content->companies()->detach();

      foreach ($parsedBody["companies"] as $company) {             
         if ($asoc = Company::find($company["id"])) {
            $content->companies()->save($asoc);
         }
      }

      $content->genres()->detach();

      foreach ($parsedBody["genres"] as $genre) {
         if ($asoc = Genre::find($genre["id"])) {
            $content->genres()->save($asoc);
         }
      }

      $content->productionCountries()->detach();

      foreach ($parsedBody["production_countries"] as $country) {
         if ($asoc = Country::where('code',$country["code"])->first()) {
            $content->productionCountries()->save($asoc);
         }
      }

      $content->genres;
      $content->productionCountries;
      $content->companies;


      if ($content->save()) {
         return $this->pxApiResponse($response,true,$content,'Content updated succesfully',200);
      } else {
         return $this->pxApiResponse($response,false,[],'Error saving content',500);
      }

   }

   public function getContentSimilars($request,$response,$args) {
      $profileId = $request->getAttribute('profileId');
      $profile = Profile::find($profileId);
      $return = array();
      if ($content =Content::find($args["content_id"])) {
         if($content->similars->count()) {
            $similars = $content->similars()->inRandomOrder()->limit(18)->has('content')->get();
            foreach ($similars as $similar) {
               $translated = array();
               $translated = $this->getTranslatedContent($similar->content,$profile->lang);
               $translated["id"] = $similar->content->id;
               $translated["trailer_id"] = $similar->content->trailer_id;
               $translated["video"]["id"] = $similar->content->videos()->first()->id;
               $translated["video"]["duration"] = $similar->content->videos()->first()->duration;
               array_push($return,$translated);
            }
         } else {
         }
         return $this->pxApiResponse($response,true,$return,'similars',200);
      } else {
         return $this->pxApiResponse($response,false,$return,'No content',200);
      }
   }

   public function getContentById($request,$response,$args) {
      $profileId = $request->getAttribute('profileId');
      if($profileId) {
         $profile = Profile::find($profileId);
      }

      $content = Content::where("id",$args["id"])->first();
      $content->productionCountries;
      $content->companies;      
      $content->backdrops;
      $content->posters;
      $content->logos;
      $content->genres;
      $content->actors;
      $content->videos = $content->videos()->whereNotNull("published_date")->orderBy('season')->orderBy('episode')->get();
      foreach ($content->videos as $video) {
         $video->qualitys;
         $video->audios;
         $video->subtitles;
         $video->server;
         if($profile) {
            $video->event = Event::where("content_video_id",$video->id)->where("profile_id",$profile->id)->latest("updated_at")->first();
         }
      }

      if($profile){

         $content->translation = $this->getTranslatedContent($content,$profile->lang);
         if($favorite = Profile::find($profileId)->favorites->where("content_id",$content->id)->count()) {
            $content->favorite = true;
         } else {
            $content->favorite = false;
         }

         $content->event = $this->getLastContentLog($profile->id,$content->id);

         //seasons
         if($content->type == "serie") {
            $returnSeasons = array();
            $seasons = $content->videos()->select("season")->groupBy("season")->whereNotNull("published_date")->get();
            foreach ($seasons as $season) {
               array_push($returnSeasons,array(
                  "id" => $season->season,
                  "name" => "Temporada " . $season->season
               ));
            }
            $content->seasons = $returnSeasons;
         }

      }
      return $this->pxApiResponse($response,true,$content,'Content',200);
   }

   public function getContentConfig($request,$response) {

      $return = array(
         "mkv_url" => $_ENV["MKV_URL"]
      );

      return $this->pxApiResponse($response,true,$return,'Content',200);
   }

   public function enableServer($request,$response,$args) {

      try {
         $server = EncodingServer::find($args["server_id"]);
         $server->status = 1;
         $server->status_message = null;
         $server->save();

         $jobs = ChronProcessList::where("server_id",$server->id)->where("status","stopped")->update([
            "status" => "pending"
         ]);

         return $this->pxApiResponse($response,true,$server,'Server enabled',200);
      } catch (\Throwable $th) {
         return $this->pxApiResponse($response,false,[],'Error enabling server',500);
      }

   }

}