<?php
namespace App\Controllers;

use App\Models\ContentVideo;
use App\Models\Content;
use App\Models\ContentVideoAudio;
use App\Models\ContentVideoQuality;
use App\Models\ContentVideoSubtitle;
use App\Models\Event;
use DateTime;
use anlutro\cURL\cURL as CURL;

class ContentVideoController extends Controller
{

  public function getVideoById($request,$response,$args) {

    $video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first();
    $video->qualitys;
    $video->audios;
    $video->subtitles;

    return $this->pxApiResponse($response,true,$video,'video',200);

  }

  public function createContentVideo(Content $content,$video) {

    try {
       $content_video = $content->videos()->create(array(
          "name" => $video["name"]
          ,"folder" => $video["folder"]
          ,"filename" => $video["filename"]
          ,"download_file" => $video["download_file"]
          ,"duration" => $video["duration"]
          ,"season" => $video["season"]
          ,"episode" => $video["episode"]
       ));


       if($content_video) {
          foreach ($video["audios"] as $audio) {
             $content_video->audios()->create(array(
                "lang" => $audio["lang"]
             ));
          }
          foreach ($video["subtitles"] as $sub) {               
             $content_video->subtitles()->create(array(
                "lang" => $sub["lang"]
             ));
          }
          foreach ($video["qualitys"] as $quality) {
             $content_video->qualitys()->create(array(
                "name" => $quality["name"]
                ,"filename" => $quality["filename"]
             ));               
          }
       } else {
          return false;
       }

       return $content_video;
    } catch (\Throwable $th) {
       //throw $th->getMessage();
       return false;
    }
    
 }

  public function newContentVideo($request,$response,$args) {
    $parsedBody = $request->getParsedBody();
    if (!$content = Content::find($args["content_id"])) {
       return $this->pxApiResponse($response,false,[],'Content not found',404);
    }
    if($content->type == "movie") {
       //cannot add two videos to a movie  
    }
    if($parsedBody["videos"]) {
       foreach ($parsedBody["videos"] as $video) {
          //chequear y meter error en un array de estados            
          $this->createContentVideo($content,$video);
       }
       return $this->pxApiResponse($response,true,$content->videos,'Content video created succesfully',200);
    } else {
       return $this->pxApiResponse($response,false,[],'Video is mandatory',401);
    }
 }

  public function deleteVideo($request,$response,$args) {
    try {

       $video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first();

       Event::where("content_video_id",$video->id)->delete();
       
       foreach ($video->qualitys as $videoQuality) {
          $videoQuality->delete();
       }
       foreach ($video->audios as $videoQuality) {
          $videoQuality->delete();
       }
       foreach ($video->subtitles as $videoQuality) {
          $videoQuality->delete();
       }

       $folder = $video->folder;

       if($video->delete()) {

         $status = array(
            'deleteCallResponse' => array()
         );

         $curl = new CURL;

         $generatedHash = md5($folder . date('d-m-Y') . '-G(e@^&p93HPg(3<t');
         $call = $_ENV["SERVER_PROD_FILES"] . "files.php?path=".base64_encode($folder)."&hash=".base64_encode($generatedHash);

         $deleteStatus = $curl->get($call);
         $deleteStatusDecode = json_decode($deleteStatus,true);  

         $deleteStatusDecode["url"] = $call;
         array_push($status["deleteCallResponse"],$deleteStatusDecode);

         return $this->pxApiResponse($response,true,$status,'Content video deleted succesfully',200);
       } else {
          return $this->pxApiResponse($response,false,[],'Error deleting content video',500);
       }
    } catch (\Throwable $th) {
       return $this->pxApiResponse($response,false,[],'Error deleting content video',500);
    }      

 }

  public function publishVideo($request,$response,$args) {
    $parsedBody = $request->getParsedBody();

    foreach ($parsedBody["video_id"] as $video_id) {

       $video = ContentVideo::find($video_id);
       $video->published_date = new DateTime();
       $video->save();

    }

    return $this->pxApiResponse($response,true,[],'Videos published',200);

 }

public function deleteAudio($request,$response,$args) {

  $find = ContentVideoAudio::find($args["id"])->delete();

  return $this->pxApiResponse($response,true,[],'Audio deleted',200);

}

public function createAudio($request,$response,$args) {
  $parsedBody = $request->getParsedBody();

  $video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first();

  $video->audios()->create(array(
    "lang" => $parsedBody["lang"]
  ));

  return $this->pxApiResponse($response,true,[],'Audio created',200);
  }

public function deleteSubtitle($request,$response,$args) {

  $find = ContentVideoSubtitle::find($args["id"])->delete();

  return $this->pxApiResponse($response,true,[],'Subtitle deleted',200);

}

public function createSubtitle($request,$response,$args) {
  $parsedBody = $request->getParsedBody();
  $video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first();

  $video->subtitles()->create(array(
    "lang" => $parsedBody["lang"]
  ));

  return $this->pxApiResponse($response,true,[],'Subtitle created',200);
}

public function deleteQuality($request,$response,$args) {

  $find = ContentVideoQuality::find($args["id"])->delete();

  return $this->pxApiResponse($response,true,[],'Quality deleted',200);

}

public function createQuality($request,$response,$args) {
  $parsedBody = $request->getParsedBody();
  $video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first();

  $video->qualitys()->create(array(
    "name" => $parsedBody["name"]
    ,"filename" => $parsedBody["filename"]
  ));

  return $this->pxApiResponse($response,true,[],'Quality created',200);
}


  public function updateContentVideo($request,$response,$args) {

    $video = Content::find($args["content_id"])->videos->where("id",$args["video_id"])->first();

    $parsedBody = $request->getParsedBody();

    $video->name = $parsedBody["name"];
    $video->folder = $parsedBody["folder"];
    $video->filename = $parsedBody["filename"];
    $video->download_file = $parsedBody["download_file"];
    $video->duration = $parsedBody["duration"];
    $video->season = $parsedBody["season"];
    $video->episode = $parsedBody["episode"];    

    if ($video->save()) {
        return $this->pxApiResponse($response,true,$video,'Video updated succesfully',200);
    } else {
        return $this->pxApiResponse($response,false,[],'Error saving content',500);
    }
  }

}