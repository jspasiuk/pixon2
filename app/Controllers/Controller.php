<?php

namespace App\Controllers;

class Controller {
    public static function pxApiResponse($response,$status,$data,$message,$code) {
        return $response->withJson(['status' => $status, 'data' => $data, 'message' => $message, 'code' => $code],$code);
    }

    public static function uploadImage($key,$error,$finalname) {
        $ext_type = array('gif','jpg','jpe','jpeg','png');
        $uploads_dir = $_ENV["uploads_dir"];        
        $filename = basename($_FILES["images"]["name"][$key]);

        if(mb_strlen($filename,"UTF-8") < 225) {

            $extension = pathinfo($filename)["extension"];

            if(in_array($extension,$ext_type)) {                    
                if ($error == UPLOAD_ERR_OK) {
                    $tmp_name = $_FILES["images"]["tmp_name"][$key];
                    // basename() puede evitar ataques de denegación de sistema de ficheros;
                    // podría ser apropiada más validación/saneamiento del nombre del fichero                        
                    $finalname.= "." . $extension;
                    if ($stat = move_uploaded_file($tmp_name, "$uploads_dir/$finalname")){
                        return $finalname;
                    } else {
                        return false;
                    }
                }
            }
        }        
    }
}