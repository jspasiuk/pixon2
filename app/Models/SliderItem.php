<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderItem extends Model {
  protected $table = "slider_item";

  protected $fillable = ["content_id","order"];

  public function content() {
    return $this->belongsTo('App\Models\Content','content_id','id');
  }

}