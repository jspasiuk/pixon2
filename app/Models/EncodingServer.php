<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EncodingServer extends Model {
  protected $table = "encoding_server";


  public function pendingList() {
    return $this->hasMany('ChronProcessList','server_id','id');
  }

}