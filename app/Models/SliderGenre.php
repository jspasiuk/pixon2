<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderGenre extends Model {
  protected $table = "slider_genre";
  
  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;

}