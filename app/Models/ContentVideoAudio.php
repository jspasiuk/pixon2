<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentVideoAudio extends Model {
  
  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;
  protected $table = "content_video_audio";

  protected $fillable = ['lang'];

}