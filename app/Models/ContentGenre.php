<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentGenre extends Model {
  protected $table = "content_genre";  

  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;
}