<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentBackdrop extends Model {
  protected $table = "content_backdrop";

  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;

  protected $fillable = ['url'];

  
}