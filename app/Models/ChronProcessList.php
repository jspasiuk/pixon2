<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChronProcessList extends Model {
  protected $table = "chron_process_list";

  public function step() {
    return $this->hasOne('App\Models\ChronProcessStep','step','current_step');
  }

  public function content() {
    return $this->hasOne('App\Models\Content','id','content_id');
  }

  public function server() {
    return $this->hasOne('App\Models\EncodingServer','id','server_id');
  }

}