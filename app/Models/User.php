<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model{

   protected $table = 'user';
   protected $hidden = ['password'];
   protected $fillable = ['username','password'];

   public function profile() {
      return $this->hasMany('\App\Models\Profile','user_id','id');
   }

   public function device() {
      return $this->hasMany('\App\Models\UserDevice','user_id','id');
   }

}