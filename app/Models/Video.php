<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {
  protected $table = "videos";

  public function quality() {
    return $this->hasMany('\App\Models\Quality','content_video_id','id');
  }

  public function proxy() {
    return $this->hasOne('\App\Models\Proxy','proxy_id','id');
  }

}