<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {
  protected $table = "company";

  protected $hidden = ["created_at"];

  public function content() {
    //this has many content associated trought content_company
  }

}