<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentCompany extends Model {
  protected $table = "content_company";

  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;
  
}