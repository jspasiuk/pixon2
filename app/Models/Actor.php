<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model {
  protected $table = "actor";

  protected $hidden = ["created_at","updated_at"];


  public function content() {
    return $this->hasManyThrough('App\Models\Actor','content_actor','content_id','actor_id');
  }

}