<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {
  protected $table = "slider";
  const UPDATED_AT = NULL;

  public function items() {
    return $this->hasMany('App\Models\SliderItem','slider_id','id');
  }

  public function genres() {
    return $this->belongsToMany('App\Models\Genre','slider_genre','slider_id','genre_id');
  }

}