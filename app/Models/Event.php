<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {
  protected $table = "event";

  protected $hidden = ["created_at","updated_at"];

  protected $fillable = ["time","content_video_id","content_id","profile_id","type"];

  public function content() {
    return $this->belongsTo('App\Models\Content',"content_id","id");
  }

  public function video() {
    return $this->belongsTo('App\Models\ContentVideo',"content_video_id","id");
  }

}