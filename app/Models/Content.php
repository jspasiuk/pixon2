<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model {
  protected $table = "content";

  protected $hidden = ["created_at","updated_at"];


  public function actors() {
    return $this->belongsToMany('App\Models\Actor','content_actor','content_id','actor_id')->withPivot("job","order");
  }

  public function videos() {
    return $this->hasMany('App\Models\ContentVideo','content_id','id');
  }

  public function genres() {
    return $this->belongsToMany('App\Models\Genre','content_genre','content_id','genre_id');
  }

  public function posters() {
    return $this->hasMany('App\Models\ContentPoster','content_id');
  }

  public function logos() {
    return $this->hasMany('App\Models\ContentLogo','content_id');
  }

  public function backdrops() {
    return $this->hasMany('App\Models\ContentBackdrop','content_id');
  }

  public function companies() {
    return $this->belongsToMany('App\Models\Company','content_company','content_id','company_id');
  }

  public function similars() {
    return $this->hasMany('App\Models\ContentSimilar','content_id');
  }

  public function recommendations() {
    return $this->hasMany('App\Models\ContentRecommendation','content_id');
  }

  public function productionCountries() {
    //return $this->hasMany('App\Models\ContentProductionCountry','content_id');
    return $this->belongsToMany('App\Models\Country','content_production_country','content_id','country_code',null,"code");

  }
  

}