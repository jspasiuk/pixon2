<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentSimilar extends Model {
  protected $table = "content_similar";

  public function content() {
    return $this->hasOne('App\Models\Content','tmdb_id','similar_tmdb_id');
  }
  
}