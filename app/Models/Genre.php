<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model {
  protected $table = "genre";

  protected $hidden = ["created_at"];

  public function content() {
    //this has many content associated trought content_genre
  }

}