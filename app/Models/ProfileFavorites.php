<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileFavorites extends Model {
  protected $table = "profile_favorites";

  protected $fillable = ["profile_id","content_id"];  

  public function content() {
    return $this->belongsTo('App\Models\Content',"content_id","id");
  }

}