<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Error extends Model {
  use SoftDeletes;

  protected $table = "error";

  protected $hidden = ["updated_at"];


  public function content() {
    return $this->hasMany('App\Models\Content','id','content_id');
  }

  public function profile() {
    return $this->hasMany('App\Models\Profile','id','profile_id');
  }

  public function video() {
    return $this->hasMany('App\Models\ContentVideo','id','video_id');
  }

}