<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
  protected $table = "countries";


  public function content() {
    return $this->hasManyThrough('App\Models\Content','content_production_company','content_id','code');
  }

}