<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentVideo extends Model {
  protected $table = "content_video";
  protected $fillable = ['proxy_id','name','filename','download_file','duration','season','episode'];

  public function qualitys() {
    return $this->hasMany('App\Models\ContentVideoQuality','video_id','id');
  }

  public function audios() {
    return $this->hasMany('App\Models\ContentVideoAudio','video_id','id');
  }

  public function subtitles() {
    return $this->hasMany('App\Models\ContentVideoSubtitle','video_id','id');
  }

  public function content() {
    return $this->hasOne('App\Models\Content','id','content_id');
  }

  public function server() {
    return $this->hasOne('App\Models\ContentServer','id','content_server_id');
  }

}