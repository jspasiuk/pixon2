<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model{

   protected $table = 'user_devices';
   protected $fillable = ['device_id','user_id','pin','device_name'];

   public function user() {
      return $this->hasOne('App\Models\User','id','user_id');
   }

}