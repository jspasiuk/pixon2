<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentPoster extends Model {
  protected $table = "content_poster";

  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;

  protected $fillable = ['url','lang'];
  
}