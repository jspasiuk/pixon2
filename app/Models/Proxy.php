<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model {
  protected $table = "proxy";

  protected $hidden = ["created_at","updated_at"];


  public function video() {
    return $this->hasOne('App\Models\Proxy','proxy_id','id');
  }

}