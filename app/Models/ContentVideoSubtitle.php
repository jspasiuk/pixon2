<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentVideoSubtitle extends Model {
  
  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;
  protected $table = "content_video_subtitle";

  protected $fillable = ['lang'];

}