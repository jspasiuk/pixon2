<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentProductionCountry extends Model {
  protected $table = "content_production_country";

  const UPDATED_AT = NULL;
  const CREATED_AT = NULL;

  protected $fillable = ["country_code"];
  
}