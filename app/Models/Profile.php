<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {
  protected $table = "profile";

  public function favorites() {
    return $this->hasMany("App\Models\ProfileFavorites",'profile_id','id');
  }

  public function events() {
    return $this->hasMany("App\Models\Event",'profile_id','id');
  }

  public function user() {
    return $this->hasOne("App\Models\User",'id','user_id');
  }

}