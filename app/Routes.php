<?php

$app->group('/v1', function() {

  #error
  $this->post('/error', 'ErrorController:newError')->setName('newError');

  #register  
  $this->get('/getDeviceToken', 'UserController:getDeviceToken')->setName('getDeviceToken');
  $this->post('/registerDevice', 'UserController:registerDevice')->setName('registerDevice');
  $this->post('/validatePin', 'UserController:validatePin')->setName('validatePin');

  #devices
  $this->get('/user/info', 'UserController:getUserInfo')->setName('getUserInfo');
  $this->delete('/devices', 'UserController:deleteDevice')->setName('deleteDevice');

  #profiles
  $this->get('/profiles', 'UserController:getProfiles')->setName('getProfiles');
  $this->post('/profiles', 'UserController:newProfile')->setName('newProfile');
  $this->delete('/profiles', 'UserController:deleteProfile')->setName('deleteProfile');
  $this->put('/profiles', 'UserController:updateProfile')->setName('updateProfile');
  $this->get('/getAvatars', 'UserController:getAvatars')->setName('getAvatars');

  $this->delete('/profile/event/{content_id}', 'ContentController:deleteProfileEvent')->setName('deleteProfileEvent');

  #login menu search
  $this->get('/content/menu/{home_type}', 'ContentController:getMenu')->setName('getMenu');

  #content
  $this->post('/content/{content_id}/play/{video_id}', 'ContentController:playContent')->setName('playContent');
  $this->post('/content/{content_id}/download/{video_id}', 'ContentController:downloadContent')->setName('downloadContent');
  $this->post('/content/updateEventContent', 'ContentController:updateEventContent')->setName('updateEventContent');
  $this->get("/content/", 'ContentController:searchContent')->setName("searchContent");
  $this->get('/content/all/{id}', 'ContentController:getContentById')->setName('getContentById');

  #favorites
  $this->get('/getFavorites', 'ContentController:getFavorites')->setName('getFavorites');
  $this->post('/saveContentToProfile', 'ContentController:saveContentToProfile')->setName('saveContentToProfile');
  $this->delete('/deleteContentToProfile', 'ContentController:deleteContentToProfile')->setName('deleteContentToProfile');  

  #similars
  $this->get('/content/{content_id}/similars', 'ContentController:getContentSimilars')->setName('getContentSimilars');

  #admin
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $this->group('/admin', function() {

    $this->get('/error', 'ErrorController:show')->setName('showErrors');
    $this->delete('/error/{id}', 'ErrorController:delete')->setName('deleteError');

    $this->get('/getAdminToken', 'UserController:getAdminToken')->setName('getAdminToken');

    $this->get('/content/filtered', 'ContentController:getContentFilteredList')->setName('getContentFilteredList');
    #queue
    $this->get('/server', 'ContentController:getEncodingServers')->setName('getEncodingServers');
    $this->put('/server/{server_id}/disable', 'ContentController:disableServer')->setName('disableServer'); //TODO: add this and send a email
    $this->put('/server/{server_id}/enable', 'ContentController:enableServer')->setName('enableServer');

    $this->delete('/content/{id}', 'ContentController:deleteContent')->setName('deleteContent');  

    $this->get('/content/config', 'ContentController:getContentConfig')->setName('getContentConfig');
    $this->get('/content/all/{id}', 'ContentController:getContentById')->setName('getContentById');
    $this->get('/content/{status}', 'ContentController:getContentList')->setName('getContentList');    
    $this->get('/content/company/all', 'ContentController:getCompany')->setName('getCompany');  
    $this->get('/content/video/proxy/all', 'ContentController:getProxy')->setName('getProxy');
    $this->put('/content/{id}', 'ContentController:updateContent')->setName('updateContent');
    $this->post('/content/new', 'ContentController:newContent')->setName('newContent');
    $this->get("/content/", 'ContentController:searchContent')->setName("searchContent");
  
    #company
    $this->delete('/content/{content_id}/company/{id}', 'ContentController:deleteCompany')->setName('deleteCompany');
    $this->post('/content/{content_id}/company', 'ContentController:createCompany')->setName('createCompany');
  
    #country
    $this->get('/content/countries/all', 'ContentController:getCountries')->setName('getCountries');
    $this->delete('/content/{content_id}/country/{code}', 'ContentController:deleteCountry')->setName('deleteCountry');
    $this->post('/content/{content_id}/country', 'ContentController:createCountry')->setName('createCountry');
  
    #logo
    $this->delete('/content/{content_id}/logo/{id}', 'ContentController:deleteLogo')->setName('deleteLogo');

    #poster
    $this->delete('/content/{content_id}/poster/{id}', 'ContentController:deletePoster')->setName('deletePoster');
    $this->post('/content/{content_id}/poster', 'ContentController:createPoster')->setName('createPoster');
  
    #backdrop
    $this->delete('/content/{content_id}/backdrop/{id}', 'ContentController:deleteBackdrop')->setName('deleteBackdrop');
    $this->post('/content/{content_id}/backdrop', 'ContentController:createBackdrop')->setName('createBackdrop');
  
    #genre
    $this->get('/content/genre/all', 'ContentController:getGenre')->setName('getGenre');
    $this->delete('/content/{content_id}/genre/{genre_id}', 'ContentController:deleteGenre')->setName('deleteGenre');
    $this->post('/content/{content_id}/genre', 'ContentController:createGenre')->setName('createGenre');
  
    #actor
    $this->get('/content/actor/all', 'ContentController:getActor')->setName('getActor');
    $this->delete('/content/{content_id}/actor/{actor_id}', 'ContentController:deleteActor')->setName('deleteActor');
    $this->post('/content/{content_id}/actor', 'ContentController:createActor')->setName('createActor');
  
  
    $this->delete('/content/queue/{id}', 'ContentController:deleteProcessList')->setName('deleteProcessList');
    $this->delete('/file', 'ContentController:deleteFile')->setName('deleteFile');
    $this->post("/content/queue","ContentController:saveProcessList")->setName("saveProcessList");  
  
    #videos
    $this->get('/content/{content_id}/video/{video_id}', 'ContentVideoController:getVideoById')->setName('getVideoById');
  
    $this->delete('/content/{content_id}/video/{video_id}', 'ContentVideoController:deleteVideo')->setName('deleteVideo');
    $this->delete('/content/{content_id}/video/{video_id}/subtitle/{id}', 'ContentVideoController:deleteSubtitle')->setName('deleteSubtitle');
    $this->delete('/content/{content_id}/video/{video_id}/audio/{id}', 'ContentVideoController:deleteAudio')->setName('deleteAudio');
    $this->delete('/content/{content_id}/video/{video_id}/quality/{id}', 'ContentVideoController:deleteQuality')->setName('deleteQuality');
  
    $this->put('/content/{content_id}/video/{video_id}', 'ContentVideoController:updateContentVideo')->setName('updateContentVideo');
    $this->put("/content/video/publish","ContentVideoController:publishVideo")->setName("publishVideo");
  
    $this->post('/content/{content_id}/video', 'ContentVideoController:newContentVideo')->setName('newContentVideo');
    $this->post('/content/{content_id}/video/{video_id}/subtitle', 'ContentVideoController:createSubtitle')->setName('createSubtitle');
    $this->post('/content/{content_id}/video/{video_id}/audio', 'ContentVideoController:createAudio')->setName('createAudio');
    $this->post('/content/{content_id}/video/{video_id}/quality', 'ContentVideoController:createQuality')->setName('createQuality');
  
    #slider
    $this->get('/slider/config', 'SliderController:getSliderConfig')->setName('getSliderConfig');
    $this->get('/slider/all', 'SliderController:getSliders')->setName('getSliders');  
    $this->get("/slider/{id}", 'SliderController:getSliderById')->setName('getSliderById');
    $this->post('/slider', 'SliderController:newSlider')->setName('newSlider');
    $this->delete('/slider/{id}', 'SliderController:deleteSlider')->setName('deleteSlider');
    $this->delete('/slider/{slider_id}/item/{id}', 'SliderController:deleteItem')->setName('deleteItem');
    $this->put('/slider/{id}', 'SliderController:updateSlider')->setName('updateSlider');
    $this->post('/uploadFile', 'UserController:uploadFileImage')->setName('uploadFile');

  })->add(new UserAuthMiddleware());

});

$app->group('/sales',function() {
  //custom route with custom middleware, using api_key auth
  $this->get('/customer/{saler_id}', 'UserController:getUsersForSaler')->setName('getUsersForSaler');
  $this->get('/customer', 'UserController:getUsersFiltered')->setName('getUsersFiltered');
  
  $this->post('/customer', 'UserController:newUsersForSaler')->setName('newUsersForSaler');
  $this->put('/customer', 'UserController:updateUserForSaler')->setName('updateUserForSaler');
  $this->delete('/customer/{id}', 'UserController:deleteUserForSaler')->setName('deleteUserForSaler');
})->add(function ($request, $response, $next) {

  if($request->getHeader('authorization')[0] === $_ENV["SALES_API_KEY"]) {
    return $next($request, $response);
  }
  else {
    return $response->withJson(['status' => false, 'data' => [], 'message' => 'Invalid Key','code'=>'401'],401);
  }
});;

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
  $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
  return $handler($req, $res);
});