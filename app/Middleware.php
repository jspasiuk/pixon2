<?php

use App\Models\User as User;

class UserAuthMiddleware
{
    public function __construct() {
        
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        $route = $request->getAttribute('route');
        if(!$route){
            return $response->withJson(['status' => false, 'data' => [], 'message' => 'Invalid route','code'=>'404'],404);
        }

        $routeName = $route->getName();               

        if($routeName === 'getAdminToken') {
            // Proceed as normal...
            return $next($request, $response);
        }
    
        $decoded = $request->getAttribute("token"); 
    
        $user = User::find($decoded["user_id"]);
            
        if($user->is_admin) {
            $response = $next($request, $response);
        } else {
            return $response->withJson(['status' => false, 'data' => [], 'message' => 'Invalid Role / Permission','code'=>'401'],401);
        }

        return $response;
    }
}