<?php

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/app/Middleware.php';
// A provider for returning the responses

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$app = new \Slim\App([
  'settings' => [
    'displayErrorDetails' => false,
    'addContentLengthHeader' => false,
    'determineRouteBeforeAppMiddleware' => true,
     'db' => [
        'driver' => 'mysql',
        'host' => $_ENV["dbhost"],
        'database' => $_ENV["dbname"],
        'username' => $_ENV["dbuser"],
        'password' => $_ENV["dbpass"],
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci'
        ]
    ]
]);

$container = $app->getContainer();
//boot eloquent connection
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
//pass the connection to global container (created in previous article)
$container['db'] = function ($container) use ($capsule){
   return $capsule;
};

//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
//header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

$container['errorHandler'] = function ($c) {
  return function ($request, $response, $exception) use ($c) {    
    return $response->withJson(
      [
      'status' => false
      , 'data' => []
      , 'message' => 'Ha ocurrido un error fatal.'
      , "code" => 500
      , "message" => $exception->getMessage()
      ]);
  };
};

$container['phpErrorHandler'] = function ($c) {
  return function ($request, $response, $exception) use ($c) {    
    return $response->withJson(
      [
      'status' => false
      , 'data' => []
      , 'message' => 'Ha ocurrido un error fatal.'
      , "code" => 500
      , "message" => $exception->getMessage()
      ]);
  };
};

$container['UserController'] = function ($container) {
   return new \App\Controllers\UserController($container);
};

$container['ContentController'] = function ($container) {
  return new \App\Controllers\ContentController($container);
};

$container['ContentVideoController'] = function ($container) {
  return new \App\Controllers\ContentVideoController($container);
};

$container['SliderController'] = function ($container) {
  return new \App\Controllers\SliderController($container);
};


$container['ErrorController'] = function ($container) {
  return new \App\Controllers\ErrorController($container);
};


// register view template engine and configurations 
$container['templates'] = function () {
  return new Mustache_Engine(array(
    'loader' => new Mustache_Loader_FilesystemLoader(dirname(__DIR__) . '/app/Views/',array('extension' => '.html')),
    'escape' => function($value) {
        return htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
    }
    ,'extension' => '.html'
  ));
};

$app->add(new Tuupola\Middleware\JwtAuthentication([
  "secure" => false,
  "relaxed" => ["localhost"],
  "secret" => $_ENV["jwt_secret"],
  "ignore" => ["/sales","/v1/admin/getAdminToken","/v1/testpath","/v1/getStatus","/v1/registerDevice","/v1/validatePin","/v1/getDeviceToken"],
  "error" => function ($response, $arguments) {    
    return $response->withJson(['status' => false, 'data' => [], 'message' => $arguments["message"], "code" => 401],401);
  },
  "before" => function ($request, $arguments) {
    //fill the user permissions for id-barrio
    $profileID = $request->getHeader('profileId')[0];
    if(!$lang = $request->getHeader('lang')[0]) {
      $lang = "es";
    }
    return $request->withAttribute("profileId", $profileID)->withAttribute("lang",$lang);
  }
]));

require dirname(__DIR__) . '/app/Routes.php';
